Class {
	#name : #BaselineOfPhineas,
	#superclass : #BaselineOf,
	#category : #BaselineOfPhineas
}

{ #category : #baselines }
BaselineOfPhineas >> baseline: spec [
	<baseline>
	spec
		for: #common
		do: [
			spec
				package: 'Phineas';
				package: 'Phineas-Tests-Project' with: [ spec requires:#('Phineas') ];
				package: 'Phineas-UI' with: [ spec requires:#('Phineas') ];
				package: 'Phineas-Tests' with: [ spec requires:#('Phineas-Tests-Project') ] ].
]
