Class {
	#name : #PITypeInspector,
	#superclass : #SpPresenter,
	#instVars : [
		'root',
		'dependencyTree'
	],
	#category : #'Phineas-UI'
}

{ #category : #specs }
PITypeInspector class >> defaultSpec [
	
	^ SpBoxLayout newVertical
		add: #dependencyTree;
		yourself.

]

{ #category : #accessing }
PITypeInspector >> dependencyTree [

	^ dependencyTree
]

{ #category : #initialization }
PITypeInspector >> doubleClickOn: aSelection [

	(dependencyTree itemAtPath: aSelection selectedPath) inspect 
]

{ #category : #initialization }
PITypeInspector >> initializePresenters [

	dependencyTree := self newTreeTable
		addColumn: (SpStringTableColumn evaluated: [:e | e printString ]);
		children: [ :anItem | anItem types asOrderedCollection ];
		activateOnDoubleClick;
		whenActivatedDo: [ :aSelection | self doubleClickOn: aSelection];
		yourself
]

{ #category : #accessing }
PITypeInspector >> root: aPIMessageType [ 

	root := aPIMessageType.
	dependencyTree roots: { root } 
]
