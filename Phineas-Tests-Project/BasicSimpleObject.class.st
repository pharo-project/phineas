Class {
	#name : #BasicSimpleObject,
	#superclass : #BasicObject,
	#instVars : [
		'a',
		'b'
	],
	#category : #'Phineas-Tests-Project'
}

{ #category : #accessing }
BasicSimpleObject >> a [
	^ a
]

{ #category : #accessing }
BasicSimpleObject >> a: anObject [
	a := anObject
]

{ #category : #accessing }
BasicSimpleObject >> b [
	^ b
]

{ #category : #accessing }
BasicSimpleObject >> b: anObject [
	b := anObject
]
