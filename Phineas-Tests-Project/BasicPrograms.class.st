Class {
	#name : #BasicPrograms,
	#superclass : #Object,
	#category : #'Phineas-Tests-Project'
}

{ #category : #accessing }
BasicPrograms >> printMixingTypes [
	"Basic example of translation"
	"Manages temporary variable declarations, instantiation, and message sends"
	| io result |

	io := BasicIO new.
	result := 1 + 1.
	
	io print: 'Hello World!!!'.
	io print: '1 + 1 = '.
	io print: result.
	
	io freeObject.
	
	^ 0
]

{ #category : #accessing }
BasicPrograms >> printStrings [
	"Basic example of translation"
	"Manages temporary variable declarations, instantiation, and monomorphic message sends"
	| io |

	io := BasicIO new.	
	io print: 'Hello World!!!'.
	
	io freeObject.
	
	^ 0
]

{ #category : #accessing }
BasicPrograms >> returningResultOfSum [
	"Basic example of translation"
	"Sum 2 int constants and return the value"
	
	^ 1 + 2

]

{ #category : #accessing }
BasicPrograms >> simpleIfConstruction [
	"Basic example of translation"
	"Manages Simple If constructions"
	| a b |
	
	a := 3.
	b := 0.
	
	a = 1 ifTrue: [ ^ -1 ].
	a ~= 1 ifFalse: [ ^ -1 ].
	
	a > b ifTrue: [ ^ b ] ifFalse: [ ^ a ].

]

{ #category : #accessing }
BasicPrograms >> simpleObject [
	| obj returnValue |
	
	obj := BasicSimpleObject new.
	obj a: 1.
	obj b: 0.
	
	returnValue := obj b.
	
	obj freeObject.
	
	^ returnValue
]

{ #category : #accessing }
BasicPrograms >> unionTypeInVariable [

	| a |
	
	a := 'Something'.
	a := 1.
	
	^ a 
]
