Class {
	#name : #BasicIO,
	#superclass : #BasicObject,
	#category : #'Phineas-Tests-Project'
}

{ #category : #'as yet unclassified' }
BasicIO >> basicPutString: aString [
	^ self ffiCall: #(int puts (String aString))
]

{ #category : #'library path' }
BasicIO >> ffiLibraryName [
	^ LibC
]

{ #category : #printing }
BasicIO >> print: anObject [

	self basicPutString: anObject asString.
]
