Class {
	#name : #PIConcreteType,
	#superclass : #PIAbstractConcreteType,
	#category : #'Phineas-Types'
}

{ #category : #comparing }
PIConcreteType >> = anotherType [
	^ self species = anotherType species and: [ self concreteClass = anotherType concreteClass ]
]

{ #category : #visiting }
PIConcreteType >> accept: aVisitor [
	^ aVisitor visitConcreteType: self
]

{ #category : #comparing }
PIConcreteType >> anInstanceOfElementType [
	| ecc |
	"When infering a collection, we have to infer some methods on all element, without having access to them. this method provides a standart stub so the inference is applyed to the right kind of argument."
	ecc := self concreteClass.
	ecc = SmallInteger
		ifTrue: [ ^ RBLiteralNode value: 1 ].
	ecc = Boolean
		ifTrue: [ ^ RBLiteralNode value: true ].
	ecc = Character
		ifTrue: [ ^ RBLiteralNode value: $a ].
	ecc = ByteString
		ifTrue: [ ^ RBLiteralNode value: 'a' ].
	ecc = Symbol
		ifTrue: [ ^ RBLiteralNode value: #a ].
	ecc = Float
		ifTrue: [ ^ RBLiteralNode value: 1.1 ].
	self halt. "Want to be aware when it won't be enough, to know if I could improve this"
	^ ecc new
]

{ #category : #visiting }
PIConcreteType >> at: aSymbol using: typesProvider [
	^ ((typesProvider explicitTypeHeuristic new at: aSymbol) asPhineasType: typesProvider)
]

{ #category : #accessing }
PIConcreteType >> concreteClass [
	^ concreteClass
]

{ #category : #variables }
PIConcreteType >> fillType: typesProvider [
	concreteClass class methodDict detect: [:method| method selector = #declareCVarsIn: ] "add types based on what's declared in slang"
		ifFound:[:declareCVarsInMethod| self getBindingFromClassSide: declareCVarsInMethod ast using: typesProvider ].

	concreteClass slots do:[ :aSlot | 
		instanceVariables at: aSlot name 	ifPresent:[" self halt "]
			ifAbsentPut:[
				"If they aren't already filled in the slang thing"
				PIVariableType new owner: self ; name: aSlot name; yourself ]].
	
	concreteClass classVariables do: [ :aClassVar | classVariables at: aClassVar name put: (typesProvider variableTypeFromClasses: {aClassVar value class}) ].

	poolDictionaries := concreteClass sharedPools collect: [ :e | typesProvider typeForClass: e ].
]

{ #category : #variables }
PIConcreteType >> fillType: typesProvider fromObject: anObject [
	concreteClass allSlots do: [ :aSlot | (self instanceVariable: aSlot name) addType: (typesProvider typeForObject: (anObject readSlot: aSlot)) ].	
	concreteClass classVariables do: [ :aClassVar | (classVariables at: aClassVar name) addType: (typesProvider variableTypeFromClasses: {aClassVar value class}) ].
	
	self consolidateTypes.
	
	poolDictionaries := concreteClass sharedPools collect: [ :e | typesProvider typeForClass: e ].
	
	superclassType ifNotNil:[ superclassType fillType: typesProvider fromObject: anObject ].
]

{ #category : #comparing }
PIConcreteType >> getBindingFromClassSide: aMethodNode using: typesProvider [
	aMethodNode statements
		do:
			[ :aStatement |
			aStatement isCascade
				ifTrue: [ aStatement messages do:[:aMessage|
						(aMessage selector = #var:type: or:[ aMessage selector = #var:declareC: ])ifTrue:[
							"self halt."
							instanceVariables add: ((aMessage arguments at: 1) value -> (PIExplicitType new 
								owner: self ;
								name: (aMessage arguments at: 1) value ;
								explicitType: (self at: (aMessage arguments at: 2) value using: typesProvider);
								yourself))
						]]].
			aStatement isMessage ifTrue:[
					(aStatement selector = #var:type: or:[ aStatement selector = #var:declareC: ]) ifTrue:[

						instanceVariables add: ((aStatement arguments at: 1) value  -> 
						(PIExplicitType new 
								owner: self ;
								name: ((aStatement arguments at: 1) value) ;
								explicitType: (self at: (aStatement arguments at: 2) value using: typesProvider);
								yourself)) ]]].
]

{ #category : #comparing }
PIConcreteType >> hash [
	^ self concreteClass className hash
]

{ #category : #testing }
PIConcreteType >> isConcreteType [
	^ true
]

{ #category : #printing }
PIConcreteType >> printOnInner: aStream [
	concreteClass printOn: aStream
]

{ #category : #comparing }
PIConcreteType >> types [
	^ {  }
]
