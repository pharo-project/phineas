Class {
	#name : #PISmallIntegerMethodType,
	#superclass : #PIMethodType,
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PISmallIntegerMethodType >> accept: aVisitor [ 

	^ aVisitor visitSmallIntegerMethodType: self
]

{ #category : #testing }
PISmallIntegerMethodType >> isPrimitive [ 
	^self node pragmas anySatisfy: [:aPragma| aPragma isPrimitive ]
]
