Class {
	#name : #PINodeTypeVisitor,
	#superclass : #Object,
	#instVars : [
		'inferer'
	],
	#category : #'Phineas-Visitor'
}

{ #category : #visiting }
PINodeTypeVisitor >> getTypeForVariableNode: aNode declaredIn: aMethodOrBlock [
	| phineasMorB |
	phineasMorB := aMethodOrBlock isMethod
		ifTrue: [ inferer typeProvider methodTypes
				detect: [ :aMethodType | aMethodType node = aMethodOrBlock ] ]
		ifFalse: [ inferer typeProvider blockTypes
				detect: [ :aBlockType | aBlockType node = aMethodOrBlock ] ].
	^ (phineasMorB
		variableAt: aNode name asSymbol
		ifAbsent: [ self
				error: NoTypeFound signal ])
		ifNotNil: [ :aVariable | aVariable ]
]

{ #category : #visiting }
PINodeTypeVisitor >> inferer [
	^ inferer
]

{ #category : #visiting }
PINodeTypeVisitor >> inferer: anInferer [
	^ inferer := anInferer
]

{ #category : #visiting }
PINodeTypeVisitor >> visitArgumentVariableNode: anArgumentNode [
	| methodOrBlock |
	"˜Arguments nodes are declared in methods/blocks"
	methodOrBlock := anArgumentNode whoDefines: anArgumentNode name.
	^ self getTypeForVariableNode: anArgumentNode declaredIn: methodOrBlock 
]

{ #category : #visiting }
PINodeTypeVisitor >> visitAssignmentNode: anAssignmentNode [ 
	"type of an assigment is the type of the value that is assigned to the variable"
	^ anAssignmentNode value acceptVisitor: self
]

{ #category : #visiting }
PINodeTypeVisitor >> visitBlockNode: aBlockNode [
	^ (inferer blockTypeFor: aBlockNode) returnType
]

{ #category : #visiting }
PINodeTypeVisitor >> visitClassVariableNode: aGlobalNode [
	"Works only for classes so far."
	"Globals variables such as Smalltalk / Transcript can't be supported so far."
 	^ inferer inferGlobalNode: aGlobalNode
	
]

{ #category : #visiting }
PINodeTypeVisitor >> visitGlobalVariableNode: aGlobalNode [
	"Works only for classes so far."
	"Globals variables such as Smalltalk / Transcript can't be supported so far."
 	^ inferer inferGlobalNode: aGlobalNode
	
]

{ #category : #visiting }
PINodeTypeVisitor >> visitInstanceVariableNode: anInstanceVariableNode [
	^ (inferer getMethodNodeOf: anInstanceVariableNode) receiver
		variableAt: anInstanceVariableNode name
]

{ #category : #visiting }
PINodeTypeVisitor >> visitLiteralNode: aLiteralNode [ 
	" For p7 support "
	^ self visitLiteralValueNode: aLiteralNode
]

{ #category : #visiting }
PINodeTypeVisitor >> visitLiteralValueNode: aLiteralValueNode [
	^ self inferer typeForLiteral: aLiteralValueNode value
]

{ #category : #visiting }
PINodeTypeVisitor >> visitMessageNode: aMessageNode [
	^ inferer typeProvider messageTypes
		detect: [ :each | each message = aMessageNode ]
]

{ #category : #visiting }
PINodeTypeVisitor >> visitMethodNode: aMethodNode [
	^ (inferer getMethodNodeOf: aMethodNode) returnType
]

{ #category : #visiting }
PINodeTypeVisitor >> visitSelfNode: aSelfNode [
	^ (inferer getMethodNodeOf: aSelfNode) receiver
]

{ #category : #visiting }
PINodeTypeVisitor >> visitTemporaryVariableNode: aTemporaryNode [
	| methodOrBlock |
	"temporary nodes are declared in sequence nodes"
	methodOrBlock := (aTemporaryNode whoDefines: aTemporaryNode name) parent.
	^ self getTypeForVariableNode: aTemporaryNode declaredIn: methodOrBlock
]
