Class {
	#name : #PIPerformHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIPerformHeuristics >> applyToMessage: aMessage receiver: aReceiver arguments: args [
	| fakeMessageNode newArgs |

	fakeMessageNode := RBMessageNode new.
	fakeMessageNode parent: aMessage parent.
	fakeMessageNode receiver: aMessage receiver.
	fakeMessageNode arguments: aMessage arguments allButFirst.
	fakeMessageNode selector: args first value.

	newArgs := args allButFirst collect: [ :e | { e } asSet ].

	^ self inferer inferMessage: fakeMessageNode receiver: aReceiver arguments: newArgs 
]

{ #category : #testing }
PIPerformHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [
	^ (#(perform: perform:with:) includes: aMessage selector) and: [ args first isSymbolType ]
]
