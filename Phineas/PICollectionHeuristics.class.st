Class {
	#name : #PICollectionHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PICollectionHeuristics >> applyToMessage: aMessage receiver: aReceiver arguments: args [


	(aMessage selector = #size) ifTrue: [ ^ heuristics inferer typeForClass: SmallInteger ].

	(aMessage selector = #at:) ifTrue:[
		^ aReceiver elementType	
	].
 	
	(aMessage selector = #at:put:) ifTrue: [
		aReceiver elementType addType: args second.
		^ aReceiver elementType
	].

	(#(add: addLast: addFirst:) includes: aMessage selector) ifTrue: [
		"should fail if it's an array for addLast: addFirst:. Will have to be done when this is refactored properly"
		aReceiver elementType addType: args first.
		^ aReceiver elementType
	].

	(#(first removeFirst) includes: aMessage selector) ifTrue: [
		^ aReceiver elementType
	].


	(#(sortBlock: sort) includes: aMessage selector) ifTrue: [
		^ aReceiver
	].

	aMessage selector = #detect: ifTrue:[
		^ aReceiver elementType	
	].

	aMessage selector = #collect: ifTrue:[ 
		args anyOne isSymbolType ifTrue:[|resType fakeMessageNode newArgs |
			resType := self inferer typeProvider createCollectionType: aReceiver collectionType.
			aReceiver elementType concreteTypes do:[:aType|.
				fakeMessageNode := RBMessageNode new.
				fakeMessageNode receiver: (aType anInstanceOfElementType).
				fakeMessageNode arguments: aMessage arguments allButFirst.
				fakeMessageNode selector: args first value.	
			
				newArgs := args allButFirst collect: [ :e | { e } asSet ].
				resType elementType addType: (self inferer inferMessage: fakeMessageNode receiver: aType arguments: newArgs).
			].
		^ resType]
		ifFalse:[ 
			^ PICollectionType new collectionType: aReceiver collectionType ; elementType: (args first calculateTypesWith: {aReceiver elementType}) ].
	
	].


	self halt.


]

{ #category : #'special types' }
PICollectionHeuristics >> handleSpecialTypes: aClass [
	({ Array. OrderedCollection. SortedCollection. Stack. LinkedList } includes: aClass)
		ifTrue: [ ^ heuristics inferer typeProvider createCollectionType: aClass ].
		
	^ nil
]

{ #category : #testing }
PICollectionHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [	
	aReceiver isCollectionType ifFalse: [ ^ false ].
	"#detect:ifFound: #detect:ifFound:ifNone: untreated"
	"#ifNone Untreated and does not exists"
	^ #(#add: #addFirst: #addLast: #at: #at:put: collect: #detect: #first #removeFirst #size #sort #sortBlock:) 
	includes: aMessage selector	
	
]
