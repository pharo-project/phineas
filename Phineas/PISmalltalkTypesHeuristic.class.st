Class {
	#name : #PISmalltalkTypesHeuristic,
	#superclass : #PIAbstractTypeHeuristic,
	#category : #'Phineas-Heuristics-ExplicitTypes'
}

{ #category : #'explicit-type' }
PISmalltalkTypesHeuristic >> at: aSymbol [
	"returns the class corresponding to the pragma value"
	^ (self class environment at: aSymbol ifAbsent: [ self error: aSymbol]) asConstraint 
]

{ #category : #variables }
PISmalltalkTypesHeuristic >> returnTypeSelectors [
	^ #( #returnType: )
]

{ #category : #variables }
PISmalltalkTypesHeuristic >> variableSelectors [
	 ^ #( #var:type: )
]
