Class {
	#name : #PIRootContext,
	#superclass : #PIAbstractContext,
	#instVars : [
		'inferer'
	],
	#category : #'Phineas-Contexts'
}

{ #category : #accessing }
PIRootContext >> inferer [
	^ inferer
]

{ #category : #accessing }
PIRootContext >> inferer: anObject [
	inferer := anObject
]

{ #category : #testing }
PIRootContext >> isRootContext [
	^ true
]

{ #category : #acccessing }
PIRootContext >> methodType [
	^ nil
]

{ #category : #variables }
PIRootContext >> resolveVariable: aName [
	^ inferer environment at: aName
		ifPresent: [ :v | inferer typeForClass: v class ]
		ifAbsent: [ self error: 'Global variable ', aName, ' not Found.' ]
]
