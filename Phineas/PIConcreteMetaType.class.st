Class {
	#name : #PIConcreteMetaType,
	#superclass : #PIConcreteType,
	#instVars : [
		'baseType'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIConcreteMetaType >> accept: aVisitor [
	^ aVisitor visitConcreteMetaType: self
]

{ #category : #accessing }
PIConcreteMetaType >> baseType [
	^ baseType
]

{ #category : #variables }
PIConcreteMetaType >> classVariable: aName ifPresent: aBlock [
	super classVariable: aName ifPresent: aBlock.
	baseType classVariable: aName ifPresent: aBlock
]

{ #category : #variables }
PIConcreteMetaType >> fillType: typesProvider [
	concreteClass slots
		do:
			[ :aSlot | instanceVariables at: aSlot name put: ( PIVariableType new owner: self ; name: aSlot name; yourself )].
	baseType := typesProvider typeForClass: concreteClass instanceSide
]
