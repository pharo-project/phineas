Class {
	#name : #PISimpleHeuristic,
	#superclass : #PIAbstractHeuristic,
	#instVars : [
		'selector',
		'receiver',
		'arguments',
		'type'
	],
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #'instance creation' }
PISimpleHeuristic class >> selector: aString receiver: aClass args: aCollection [ 
	^ self selector: aString receiver: aClass args: aCollection type: {#invalid}
]

{ #category : #'instance creation' }
PISimpleHeuristic class >> selector: selector receiver: receiver args: args type: type [
	^ self new
		selector: selector;
		receiver: receiver;
		arguments: args;
		type: type;
		yourself.
]

{ #category : #applying }
PISimpleHeuristic >> applyToMessage: aMessage receiver: aReceiver arguments: aCollection [
	^ type size = 1
		ifTrue: [ | singleType |
			singleType := type anyOne.
			singleType = #invalid
				ifTrue: [ ^ self inferer invalidType ].

			singleType = #self
				ifTrue: [ aReceiver ]
				ifFalse: [ self inferer typeForClass: singleType ] ]
		ifFalse: [ self inferer typeForClasses: type ]
]

{ #category : #accessing }
PISimpleHeuristic >> arguments [
	^ arguments
]

{ #category : #accessing }
PISimpleHeuristic >> arguments: anObject [
	arguments := anObject
]

{ #category : #testing }
PISimpleHeuristic >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [
	^ selector = aMessage selector and: [ (aReceiver isOfType: receiver) and: [ self sameArguments: args ] ]
]

{ #category : #accessing }
PISimpleHeuristic >> printOn: aStream [
	aStream
		print: self class ;
		space ;
		print: receiver ;
		<< ' >> ' ;
		print: selector
]

{ #category : #accessing }
PISimpleHeuristic >> receiver [
	^ receiver
]

{ #category : #accessing }
PISimpleHeuristic >> receiver: anObject [
	receiver := anObject
]

{ #category : #testing }
PISimpleHeuristic >> sameArguments: aCollection [
	arguments
		with: aCollection
		do: [ :a :b | 
			(b isOfType: a)
				ifFalse: [ ^ false ] ].
	^ true
]

{ #category : #accessing }
PISimpleHeuristic >> selector [
	^ selector
]

{ #category : #accessing }
PISimpleHeuristic >> selector: anObject [
	selector := anObject
]

{ #category : #accessing }
PISimpleHeuristic >> type [
	^ type
]

{ #category : #accessing }
PISimpleHeuristic >> type: anObject [
	type := anObject
]
