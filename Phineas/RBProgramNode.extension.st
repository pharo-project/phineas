Extension { #name : #RBProgramNode }

{ #category : #'*Phineas' }
RBProgramNode >> beInferredBy: aPhineasInferer [
	^ aPhineasInferer inferAST: self
]

{ #category : #'*Phineas' }
RBProgramNode >> hasReturn [
	^ self statements isNotEmpty
		and: [ self statements last containsReturn ]
]
