Class {
	#name : #PIPrimitiveMethodType,
	#superclass : #PIMethodType,
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIPrimitiveMethodType >> accept: aVisitor [ 
	^ aVisitor visitPrimitiveMethodType: self 


]
