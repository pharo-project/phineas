Class {
	#name : #PIShouldNotImplementHeuristic,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIShouldNotImplementHeuristic >> applyToMessage: aMessage receiver: aReceiver arguments: args [
	self error: 'shouldNotImplement messages send aren''t taken care off yet.'
]

{ #category : #testing }
PIShouldNotImplementHeuristic >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [

	^ aMessage selector = #shouldNotImplement
]
