Extension { #name : #BlockClosure }

{ #category : #'*Phineas' }
BlockClosure >> beInferredBy: aPhineasInferer [ 
	
	^ aPhineasInferer inferBlockClosure: self
]
