Class {
	#name : #PIImmutableType,
	#superclass : #Object,
	#instVars : [
		'creatingNodeContext'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIImmutableType >> accept: aVisitor [
	self subclassResponsibility
]

{ #category : #accessing }
PIImmutableType >> addReceivedMessage: aMsg [
	"stub.
	Only required for two object we treat the same, we don't want to have a collection for the rest of them..."
]

{ #category : #accessing }
PIImmutableType >> concreteClass [
	^ self subclassResponsibility
]

{ #category : #accessing }
PIImmutableType >> concreteClasses [
	^ { self concreteClass } asSet
]

{ #category : #flattening }
PIImmutableType >> concreteTypeVisited: visited receivedMessages: allReceivedMessages [
	| selectors |
	(visited includes: self)
		ifTrue: [ ^ #() ].

	visited add: self.

	selectors := allReceivedMessages collect: #selector.
	^ (selectors allSatisfy: [ :e | self concreteClass canUnderstand: e ])
		ifTrue: [ {self} ]
		ifFalse: [ {} ]
]

{ #category : #accessing }
PIImmutableType >> concreteTypes [
	^ { self } asSet
]

{ #category : #consolidating }
PIImmutableType >> consolidateWith: aCollection [ 
	
	aCollection add: self.
	^ aCollection
]

{ #category : #accessing }
PIImmutableType >> creatingNode [
	^ creatingNodeContext
]

{ #category : #accessing }
PIImmutableType >> creatingNode: anObject [
	creatingNodeContext := anObject
]

{ #category : #testing }
PIImmutableType >> includes: aPIConcreteType [ 
	
	^ self = aPIConcreteType
]

{ #category : #testing }
PIImmutableType >> isAssociationType [
	^ false
]

{ #category : #testing }
PIImmutableType >> isBlockType [
	^ false
]

{ #category : #testing }
PIImmutableType >> isCollectionType [
	^ false
]

{ #category : #testing }
PIImmutableType >> isConcreteType [
	^ false
]

{ #category : #testing }
PIImmutableType >> isDictionaryType [
	^ false
]

{ #category : #testing }
PIImmutableType >> isEmpty [

	^ false
]

{ #category : #testing }
PIImmutableType >> isMessageType [ 
	^ false 
]

{ #category : #testing }
PIImmutableType >> isOfType: aClass [
	^ self concreteClass includesBehavior: aClass
	
]

{ #category : #testing }
PIImmutableType >> isSymbolType [
	^ false
]

{ #category : #testing }
PIImmutableType >> isVariableType [
	^ false
]

{ #category : #printing }
PIImmutableType >> needProcessing [
	self flag: #BAD. "we just dismiss types that are not supported yet. Will have to be updated when wondering why a type doesn't appear in the blocking stuff collection"
	^ false
]

{ #category : #testing }
PIImmutableType >> printOn: aStream [
	aStream print: self class;
		<< $(.
	self printOnInner: aStream.
	aStream << $).
]

{ #category : #printing }
PIImmutableType >> printOn: aStream visited: visited [

	self printOn: aStream
]

{ #category : #testing }
PIImmutableType >> printOnInner: aStream [
	self subclassResponsibility 
]

{ #category : #testing }
PIImmutableType >> readable: concreteTypes on: aStream [
	concreteTypes size = 1
		ifTrue: [ concreteTypes anyOne printOnInner: aStream.
			^ self ].
	aStream << '{'.
	concreteTypes
		do: [ :aConcreteType | aConcreteType printOnInner: aStream ]
		separatedBy: [ aStream << ' , ' ].
	aStream << '}'
]
