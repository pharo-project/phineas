Class {
	#name : #PIInvalidType,
	#superclass : #PIImmutableType,
	#category : #'Phineas-Types'
}

{ #category : #printing }
PIInvalidType >> asInvalidType [
	^self
]

{ #category : #visiting }
PIInvalidType >> concreteTypeVisited: aCollection receivedMessages: anObject [ 

	^ #()
	
]

{ #category : #printing }
PIInvalidType >> isValid [
	^ false
]
