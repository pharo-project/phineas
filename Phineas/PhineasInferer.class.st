Class {
	#name : #PhineasInferer,
	#superclass : #Object,
	#instVars : [
		'heuristics',
		'environment',
		'visitor',
		'stack',
		'rootContext',
		'methodTasks',
		'runned',
		'getTypeVisitor',
		'explicitTypeHeuristic',
		'blockingStuff',
		'typeProvider'
	],
	#category : #Phineas
}

{ #category : #'adding type' }
PhineasInferer >> addReturnType: aType [ 
	self currentContext returnType addType: aType.
]

{ #category : #utils }
PhineasInferer >> allCombinations: aCollection [
	| first tail childCombinations |
	
	aCollection ifEmpty: [ ^ #(()) ].
	
	first := aCollection first.
	tail := aCollection allButFirst.
	
	tail ifEmpty: [ ^ first collect:[:e | {e}] ].
	
	childCombinations := self allCombinations: tail.
	
	^ first flatCollect: [ :e | childCombinations collect: [ :x | { e } , x ] ]
]

{ #category : #types }
PhineasInferer >> blockTypeFor: aBlockNode [ 
	^ self typeProvider blockTypeFor: aBlockNode 
]

{ #category : #tasks }
PhineasInferer >> blockingStuff [
	self fillBlockingStuff.
	^ blockingStuff
]

{ #category : #typing }
PhineasInferer >> concreteTypesOfInstanceVariable: instanceVarName ofClass: aClass [ 
	^ (self typeForClass: aClass) variableAt: instanceVarName ifAbsent:[NotFound signal]
]

{ #category : #contexts }
PhineasInferer >> createBlockContextFor: aPIBlockType withArguments: args [
	aPIBlockType fillArguments: args.
	
	^ PIBlockContext new
		parent: aPIBlockType homeContext;
		type: aPIBlockType;
		sender: self currentContext;
		yourself.
]

{ #category : #contexts }
PhineasInferer >> createContextForMethodType: aMethodType [
	^ PIContext new
		type: aMethodType;
		parent: rootContext;
		sender: self currentContext;		
		yourself
]

{ #category : #'accessing - context' }
PhineasInferer >> currentContext [
	^ self stack top
]

{ #category : #tasks }
PhineasInferer >> doExecuteTasks [
	| task |
	[ methodTasks isEmpty ]
		whileFalse: [ 
			(runned % 10000 = 0) ifTrue: [ 
				Stdio stdout << runned printString.
				Stdio stdout crlf.	
			].

			task := methodTasks removeFirst.
			self inferMethodActivation: task.
			runned := runned + 1 ]
]

{ #category : #types }
PhineasInferer >> emptyType [
	^ self typeProvider emptyType
]

{ #category : #contexts }
PhineasInferer >> entryPoints [
	^ self typeProvider methodTypes select:[:aMethod| aMethod node selector = #inferenceEntryPoint ]
]

{ #category : #accessing }
PhineasInferer >> environment [
	^ environment
]

{ #category : #tasks }
PhineasInferer >> executeTasks [
	runned := 0.
	[ self doExecuteTasks.
	(typeProvider messageTypes select: #needsProcessing)
		do: [ :m | self scheduleMessageInference: m ] ]
		doWhileFalse: [ methodTasks isEmpty ].
]

{ #category : #accessing }
PhineasInferer >> explicitTypeHeuristic [
	^ typeProvider explicitTypeHeuristic
]

{ #category : #accessing }
PhineasInferer >> explicitTypeHeuristic: anExplicitTypeHeuristic [
	^ typeProvider explicitTypeHeuristic: anExplicitTypeHeuristic 
]

{ #category : #'infering - API' }
PhineasInferer >> extendEnvironmentWithBindings: aSetOfBindings [
	environment := environment extendWith: aSetOfBindings.
	environment valuesDo: [ :each |
		self typeProvider typeForObject: each ].
]

{ #category : #tasks }
PhineasInferer >> fillBlockingStuff [
	| blockingMethods blockingMethodsClasses blockingMethodsDictionary |
	blockingStuff := Dictionary new.
	blockingStuff
		at: #instanceVariables
		put: ((typeProvider concreteTypes select: #needProcessing)
				collect: #variablesNeedingProcessing).
				
	blockingMethods := (typeProvider methodTypes select: #needProcessing) collect: [ :aMethod | aMethod ].
	blockingMethodsClasses := (blockingMethods collect:[:aMethod| aMethod receiver concreteClass name ]) asSet.
	blockingMethodsDictionary := Dictionary new.
	blockingMethodsClasses do:[:aClass| blockingMethodsDictionary at: aClass put: (blockingMethods select:[:aMethod| aMethod receiver concreteClass name = aClass]) ].
	blockingStuff
		at: #methods
		put: blockingMethodsDictionary.


	blockingStuff at:#messages 
		put: (typeProvider messageTypes select: #wasNotTyped
			thenCollect: [ :aMessage | aMessage ]).
]

{ #category : #'infering - methods' }
PhineasInferer >> findAlreadyProcessed: aMethod receiver: aReceiver arguments: args [
	self flag:#deadCode.
	^ self typeProvider
		findMethodTypeFor: aMethod ast
		receiver: aReceiver
		withArguments: args
		ifAbsent: [ nil ]
]

{ #category : #typing }
PhineasInferer >> getMethodNodeOf: aNode [
	| aMethodNode |
	aMethodNode := aNode isMethod
		ifTrue: [ aNode ]
		ifFalse: [ aNode methodNode ].
	^ typeProvider methodTypes
		detect: [ :aMethodType | 
			aMethodType node isMethod
				and: [ aMethodType selector = aMethodNode selector ] ]
]

{ #category : #accessing }
PhineasInferer >> getTypeVisitor [
	^ getTypeVisitor
]

{ #category : #accessing }
PhineasInferer >> getTypeVisitor: aGettertypeVisitor [
	^ getTypeVisitor := aGettertypeVisitor
]

{ #category : #accessing }
PhineasInferer >> heuristics [
	^ heuristics
]

{ #category : #accessing }
PhineasInferer >> heuristics: anObject [
	heuristics := anObject
]

{ #category : #'infering - API' }
PhineasInferer >> infer: anObject [
	
	| res |
	res := (anObject beInferredBy: self).
	^res concreteTypes
]

{ #category : #'infering - API' }
PhineasInferer >> inferAST: ast [
	| expressionType methodType context |
	typeProvider methodTypes detect:[: aMethodType| aMethodType node = ast] ifFound: [:aMethodType| ^ aMethodType returnType ].
	
	methodType := PISmalltalkMethodType new
		inferer: self;
		node: ast;
		creatingNode: self currentContext;
		receiver: (self typeProvider typeForClass: ast methodNode methodClass);
		getPragmasInformations;
		fillArguments: #();
		fillTemporaries;
		trySetReturnType;
		yourself.
	
	self typeProvider addType: methodType.
		
	context := self createContextForMethodType: methodType.
	self stack push: context.
	expressionType := ast acceptVisitor: (self visitor methodType: methodType ; yourself).
	
	self executeTasks.	
	
	^ expressionType
]

{ #category : #'infering - API' }
PhineasInferer >> inferBlockClosure: aBlockClosure [ 

	| fakeMethod oldCompilationContext |
	aBlockClosure isClean ifFalse: [ self error: 'Cannot infer non clean blocks' ].
	oldCompilationContext := aBlockClosure sourceNode methodNode compilationContext.
	fakeMethod := RBMethodNode selector: #inferenceEntryPoint arguments: {} body: aBlockClosure sourceNode body copy.
	fakeMethod compilationContext: oldCompilationContext.
	^ self inferAST: fakeMethod.
]

{ #category : #'infering - API' }
PhineasInferer >> inferExpression: source [
	
	^ self inferExpression: source binding: #()
]

{ #category : #'infering - API' }
PhineasInferer >> inferExpression: source binding: aSetOfBindings [

	| ast |
	ast := RBParser parseExpression: source.
	ast methodNode methodClass: UndefinedObject.

	self extendEnvironmentWithBindings: aSetOfBindings.
	^ self inferAST: ast methodNode
]

{ #category : #'infering - API' }
PhineasInferer >> inferGlobalNode: aGlobalNode [
	| instance methodType |
	
	methodType:=self typeProvider 
		methodTypes 
			detect:[:aMethodType|
					aMethodType selector = aGlobalNode methodNode selector].
	[^methodType receiver classVariable:  aGlobalNode name ifPresent: [:aType| aType ] ifAbsent:[self error.].] on:Error do:[
		instance := (aGlobalNode methodNode methodClass bindingOf: aGlobalNode name) value.

		^instance isClass
		ifTrue:[ self typeForClass: instance ]
		ifFalse:[ 
			aGlobalNode name = #Smalltalk "It's actually more than that. Everything that contains 
classes..."
			ifTrue:[ self error: 'Smalltalk can''t be infered, use direct reference' ].
			self inferObject: instance ].
		
		]
]

{ #category : #'as yet unclassified' }
PhineasInferer >> inferInstanceVariable: aVariableType [
	| methodUsingIV |
	self halt. " Not functionnal. infering that deep means we endup hitting something unsupported..."
	methodUsingIV := aVariableType owner concreteClass methodDict
		select: [ :aMethod | 
			(aMethod variableNodes
				anySatisfy: [ :aVar | aVar name = aVariableType name ])
				and: [ aMethod argumentNames isEmpty ] ].
	methodUsingIV do: [ :aMethod | self inferAST: aMethod ast ].
]

{ #category : #infering }
PhineasInferer >> inferInstancesVariablesOf: aClass [
	"Only usable with explicit instance variables"
	^ self typeProvider createConcreteType: aClass
]

{ #category : #'infering - messages' }
PhineasInferer >> inferMessage: aMessage receiver: receiverType arguments: argTypes [ 
	
	^ self scheduleMessageInference: (self typeProvider
		typeForMessage: aMessage
		receiver: receiverType
		arguments: argTypes
		homeContext: self currentContext)
]

{ #category : #'infering - methods' }
PhineasInferer >> inferMethodActivation: aPIMethodActivation [
	^ [ self stack push: aPIMethodActivation message homeContext.
	(self inferenceStrategyFor: aPIMethodActivation)
		applyTo: aPIMethodActivation ]
		ensure: [ self stack pop ]
]

{ #category : #'infering - API' }
PhineasInferer >> inferObject: anObject [
	
	^ self typeProvider typeForObject: anObject
]

{ #category : #'infering - API' }
PhineasInferer >> inferRootMethod: aMethod [ 
	
	^ self inferRootMethod: aMethod binding: #()
]

{ #category : #'infering - API' }
PhineasInferer >> inferRootMethod: aMethod binding: aSetOfBindings [

	self extendEnvironmentWithBindings: aSetOfBindings.
	^ self inferAST: aMethod ast.
]

{ #category : #'infering - methods' }
PhineasInferer >> inferenceStrategyFor: aPIMethodActivation [ 
	
	^ self heuristics
		heuristicFor: aPIMethodActivation
		ifNone: [ PIMethodInferer on: self ]
]

{ #category : #initialization }
PhineasInferer >> initialize [
	typeProvider := PITypesState new inferer: self; yourself.
	heuristics := PIHeuristics for: self.
		
	environment := PIGlobalEnvironment new.
	
	stack := Stack new.
	
	rootContext := (PIRootContext new 
			inferer: self;
			returnType: (PIVariableType new name: #aReturnType; yourself);
			yourself).
			
	stack push: rootContext.
	methodTasks := OrderedCollection new.
	
	getTypeVisitor := PINodeTypeVisitor new inferer: self ; yourself.

	visitor := PIVisitor  new inferer: self; yourself.
]

{ #category : #types }
PhineasInferer >> invalidType [
	^ self typeProvider invalidType
]

{ #category : #querying }
PhineasInferer >> messageTypeFromNode: aMessageNode [
	| msgs |
	msgs := self typeProvider messageTypes
		select: [ :e | e messageNode = aMessageNode ].
	self assert: msgs size = 1.
	^ msgs anyOne
]

{ #category : #query }
PhineasInferer >> messageTypeFromNode: aRBMessageNode inMethodWithType: aPIMethodType [ 
	self shouldBeImplemented.
]

{ #category : #accessing }
PhineasInferer >> methodTasks [
	^ methodTasks
]

{ #category : #typing }
PhineasInferer >> phineasTypeOfNode: anASTNode [
	"returns the type of a node as an IdentitySet"

	^ anASTNode acceptVisitor: getTypeVisitor
]

{ #category : #'stack-management' }
PhineasInferer >> pushContextWithMethod: methodAST receiver: receiverType [
	
	| methodType |
	methodType := PISmalltalkMethodType new
		node: methodAST ;
		creatingNode: self currentContext;
		receiver: receiverType;
		inferer: self;
		getPragmasInformations;
		fillArguments: #();
		fillTemporaries;
		trySetReturnType;
		yourself.
	self typeProvider addMethodType: methodType.	
	self pushContextWithMethodType: methodType.
]

{ #category : #'stack-management' }
PhineasInferer >> pushContextWithMethodType: aMethodType [
	
	self stack push: (self createContextForMethodType: aMethodType).
]

{ #category : #'infering - messages' }
PhineasInferer >> scheduleMessageInference: aPIMessageType [ 

	aPIMessageType methodActivations do: [ :methodActivation |
		self scheduleMethodInference: methodActivation ].
	^ aPIMessageType
]

{ #category : #'infering - methods' }
PhineasInferer >> scheduleMethodInference: aPIMethodActivation [
	
	aPIMethodActivation hasType
		ifTrue: [ ^ self ].
	methodTasks add: aPIMethodActivation
]

{ #category : #'accessing - context' }
PhineasInferer >> selfType [
	 ^ self currentContext receiver
]

{ #category : #accessing }
PhineasInferer >> stack [
	^ stack
]

{ #category : #types }
PhineasInferer >> typeForClass: aClass [ 
	^ self typeProvider typeForClass: aClass
]

{ #category : #types }
PhineasInferer >> typeForClasses: aCollection [ 
	^ self typeProvider variableTypeFromClasses: aCollection
]

{ #category : #types }
PhineasInferer >> typeForLiteral: aLiteral [ 
	^ self typeProvider typeForLiteral: aLiteral
]

{ #category : #typing }
PhineasInferer >> typeOf: anASTNode [ 
	
	^ anASTNode acceptVisitor: self visitor
]

{ #category : #typing }
PhineasInferer >> typeOfInstanceVariable: instanceVarName ofClass: aClass [ 
	^(self concreteTypesOfInstanceVariable: instanceVarName ofClass: aClass)
		concreteClasses
]

{ #category : #typing }
PhineasInferer >> typeOfNode: anASTNode [
	"returns the type of a node as an IdentitySet"

	^ self phineasTypeOfNode: anASTNode
]

{ #category : #accessing }
PhineasInferer >> typeProvider [
	^ typeProvider
]

{ #category : #types }
PhineasInferer >> typesOfMethod: aMethodNode [ 
	^ self typesOfMethod: aMethodNode selector from: aMethodNode methodClass
]

{ #category : #types }
PhineasInferer >> typesOfMethod: aSelector from: aClass [ 
	^ self typeProvider typesOfMethod: aSelector from: aClass
]

{ #category : #types }
PhineasInferer >> variableType: aCollection [ 
	^ self typeProvider variableType: aCollection
]

{ #category : #visiting }
PhineasInferer >> visitor [
	visitor methodType: nil. " we use a unique visitor, so we're reseting the methodType to nil when asked for."
	^ visitor
]
