Class {
	#name : #PICaseOfHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics'
}

{ #category : #applying }
PICaseOfHeuristics >> applyToMessage: aMessageNode receiver: aPIConcreteType arguments: aCollection [ 
	| blockNodes blockTypes |
	blockNodes := aMessageNode arguments first statements collect: [ :e | e arguments first ].
	blockTypes := blockNodes collect: [ :e | self types blockTypeFor: e ].
	blockTypes do: #calculateTypes.
	
	^ self types variableType: (blockTypes collect: #blockReturnType) asSet.
]

{ #category : #applying }
PICaseOfHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: arguments [ 
	^ aMessage selector = #caseOf:otherwise:
]
