Class {
	#name : #PIBlockContext,
	#superclass : #PIAbstractInnerContext,
	#category : #'Phineas-Contexts'
}

{ #category : #printing }
PIBlockContext >> methodType [
	^ type homeContext methodType
]

{ #category : #printing }
PIBlockContext >> printOn: aStream [
	aStream
		nextPutAll: self class name;
		nextPutAll: '(';
		print: self type node;
		nextPutAll: ')'
]

{ #category : #accessing }
PIBlockContext >> receiver [
	^ parent receiver
]

{ #category : #variables }
PIBlockContext >> resolveVariable: aName [
	^ type variableAt: aName ifAbsent: [ parent resolveVariable: aName ]
]

{ #category : #accessing }
PIBlockContext >> returnType [
	^ parent returnType
]
