Class {
	#name : #PIVisitor,
	#superclass : #Object,
	#instVars : [
		'inferer',
		'methodType'
	],
	#category : #'Phineas-Visitor'
}

{ #category : #accessing }
PIVisitor >> inferer [
	^ inferer
]

{ #category : #accessing }
PIVisitor >> inferer: anObject [
	inferer := anObject
]

{ #category : #visiting }
PIVisitor >> methodType [
	^ methodType
]

{ #category : #visiting }
PIVisitor >> methodType: aMethodType [
	methodType := aMethodType
]

{ #category : #visiting }
PIVisitor >> visitArgumentVariableNode: aRBArgumentNode [ 
	| type |
	type := inferer currentContext resolveVariable: aRBArgumentNode name.
	^type
]

{ #category : #visiting }
PIVisitor >> visitArrayNode: aArrayNode [
	| elementsType |
	elementsType := self inferer variableType: (aArrayNode children collect:[:e | e acceptVisitor: self]).
	^ (self inferer typeForClass: Array) elementType: elementsType.
]

{ #category : #visiting }
PIVisitor >> visitAssignmentNode: anAssignmentNode [ 
	| valueType variableType |
	valueType := anAssignmentNode value acceptVisitor: self.
	variableType := anAssignmentNode variable acceptVisitor: self.
	
	variableType addType: valueType.
	
	^ variableType
]

{ #category : #visiting }
PIVisitor >> visitBlockNode: aBlockNode [ 
	"attempt to calculate types of the blocks without arguments
	Useful for blocks containing literals for example."
	| blockType |
	blockType := self inferer blockTypeFor: aBlockNode.
	aBlockNode arguments ifEmpty: [ blockType calculateTypes ].
	^ blockType
]

{ #category : #visiting }
PIVisitor >> visitCascadeNode: aCascadeNode [ 	
	| receiverType argumentTypes piMessage |
	
	receiverType := aCascadeNode receiver acceptVisitor: self.
	
	aCascadeNode messages do: [:message| 
		argumentTypes := message arguments collect: [:each | each acceptVisitor: self ].
		piMessage := inferer inferMessage: message receiver: receiverType arguments: argumentTypes.
		methodType ifNotNil: [:aMethodType| aMethodType addMessageType: piMessage ] ].	
	
	"the last piMessage of the cascade is returned"
	^ piMessage	


]

{ #category : #visiting }
PIVisitor >> visitClassVariableNode: aVariableNode [ 

	^ self visitVariableNode: aVariableNode
]

{ #category : #visiting }
PIVisitor >> visitGlobalVariableNode: aGlobalNode [ 
	^ inferer currentContext resolveVariable: aGlobalNode name
]

{ #category : #visiting }
PIVisitor >> visitInstanceVariableNode: anInstanceVariableNode [ 
	| iv |
	iv := self inferer currentContext resolveVariable: anInstanceVariableNode name.
	iv wasQueried: true.
	iv addRequestingNodes: methodType.
	^ iv
]

{ #category : #visiting }
PIVisitor >> visitLiteralArrayNode: aLiteralArrayNode [
	| collectionType |
	
	collectionType := (self inferer typeForLiteral: aLiteralArrayNode value).
	collectionType isCollectionType ifTrue: [ 
		collectionType elementType: (self inferer variableType: (aLiteralArrayNode children collect:[:e | e acceptVisitor: self])) ].
	^ collectionType
]

{ #category : #visiting }
PIVisitor >> visitLiteralNode: aLiteralNode [ 
	" For p7 support "
	^ self visitLiteralValueNode: aLiteralNode
]

{ #category : #visiting }
PIVisitor >> visitLiteralValueNode: aLiteralValueNode [ 
	^ self inferer typeForLiteral: aLiteralValueNode value
]

{ #category : #visiting }
PIVisitor >> visitMessageNode: aMessageNode [ 
	| receiverType argumentTypes message | 
	
	receiverType := aMessageNode receiver acceptVisitor: self.
	argumentTypes := aMessageNode arguments collect: [:each | each acceptVisitor: self ].	
	message := inferer inferMessage: aMessageNode receiver: receiverType arguments: argumentTypes.
	methodType ifNotNil: [:aMethodType| aMethodType addMessageType: message].
	^ message
]

{ #category : #visiting }
PIVisitor >> visitMethodNode: aMethodNode [

	^ aMethodNode body acceptVisitor: self.

]

{ #category : #visiting }
PIVisitor >> visitReturnNode: aReturnNode [
	| returnType |
	(aReturnNode value isMessage
		and: [ (aReturnNode value selector includesSubstring: 'signal') ])
		ifTrue: [ ^ PIVariableType new owner: methodType ; name: #aReturnType ; yourself ].
	
	"This next chunk allows to type a returned variable using the explicit type of the method, if it is set.
	<returnType:#SmallInteger>
	^ a 
	a type is SmallInteger"
	(aReturnNode value isVariable and:[ methodType returnType isExplicit ]) ifTrue:[ 
		(inferer currentContext resolveVariable: aReturnNode value name) becomeExplicit: methodType returnType concreteType.
		].

	returnType := aReturnNode value acceptVisitor: self.
	
	self inferer addReturnType: returnType.
	^ returnType
]

{ #category : #visiting }
PIVisitor >> visitSelfNode: aSelfNode [ 
	^ self inferer selfType
]

{ #category : #visiting }
PIVisitor >> visitSequenceNode: aSequenceNode [
	| last |
	
	aSequenceNode statements do: [ :e | last := e acceptVisitor: self ].
	^ last ifNil: [ PIVariableType new owner: methodType ; name: #aReturnType; yourself ]
]

{ #category : #visiting }
PIVisitor >> visitSuperNode: aSuperNode [ 
	^ self visitSelfNode: aSuperNode
]

{ #category : #visiting }
PIVisitor >> visitTemporaryVariableNode: aTemporaryNode [ 
	| type |
	type := self inferer currentContext resolveVariable: aTemporaryNode name.
	type wasQueried: true.
	type addRequestingNodes: methodType.
	^type
]

{ #category : #visiting }
PIVisitor >> visitThisContextNode: aThisContextNode [
	^ inferer typeForClass: Context
]

{ #category : #visiting }
PIVisitor >> visitVariableNode: aVariableNode [ 
	| type |
	type :=inferer currentContext resolveVariable: aVariableNode name.
	type wasQueried: true.
	type addRequestingNodes: methodType.
	^type
]
