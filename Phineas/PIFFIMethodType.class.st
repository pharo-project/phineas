Class {
	#name : #PIFFIMethodType,
	#superclass : #PIMethodType,
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIFFIMethodType >> accept: aVisitor [

	^ aVisitor visitFFIMethodType: self
]
