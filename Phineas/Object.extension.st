Extension { #name : #Object }

{ #category : #'*Phineas' }
Object >> beInferredBy: aPhineasInferer [ 
	
	^ aPhineasInferer inferObject: self
]
