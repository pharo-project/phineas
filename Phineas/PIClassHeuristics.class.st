Class {
	#name : #PIClassHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIClassHeuristics >> applyToMessage: aRBMessageNode receiver: aReceiver arguments: args [ 
	^ self inferer typeForClass: aReceiver concreteClass class
	
]

{ #category : #testing }
PIClassHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: aCollection [
	^ aMessage selector = #class or: [ (aReceiver concreteClass lookupSelector: aMessage selector) = (Object >> #species) ]
]
