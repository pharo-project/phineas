Class {
	#name : #PIObjectAtPutHeuristic,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIObjectAtPutHeuristic >> applyToMessage: aMessageNode receiver: aReceiver arguments: args [
	| concreteType |
	concreteType := aReceiver concreteClass.
	
	concreteType isVariable
		ifFalse: [ ^ heuristics inferer invalidType ].
	aReceiver isCollectionType ifTrue: [ ^ aReceiver elementType ].
	
	^ heuristics inferer typeProvider typeForClass: Object.
]

{ #category : #testing }
PIObjectAtPutHeuristic >> isApplicableToMessage: aMessage receiver: aReceiver arguments: arguments [
	| concreteClass |
	concreteClass := aReceiver concreteClass.
	^((#(at: #at:put: basicAt: #basicAt:put:) includes: aMessage selector)
		and: [ (concreteClass lookupSelector: aMessage selector)
				= (Object lookupSelector: aMessage selector) ])
]
