Class {
	#name : #PIIfNilHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIIfNilHeuristics >> applyToMessage: aMessage receiver: aReceiver arguments: args [
	
	| argTypes |
	argTypes := self typeProvider variableType: (args collect: [ :each | each calculateTypesWith: { aReceiver }]).
	aMessage arguments size = 1 ifTrue: [ 
		"If the message has only one argument, we add the receiver type to the return type."
		argTypes addType: aReceiver ].
	
	^ argTypes
]

{ #category : #testing }
PIIfNilHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [ 
	^ #(ifNil: ifNotNil: ifNil:ifNotNil: ifNotNil:ifNil:) includes: aMessage selector
]
