Class {
	#name : #PIMethodActivation,
	#superclass : #Object,
	#instVars : [
		'receiverType',
		'argumentTypes',
		'message',
		'typeProvider'
	],
	#category : #'Phineas-Types'
}

{ #category : #comparing }
PIMethodActivation >> = anObject [

	self species = anObject species ifFalse: [ ^ false ].
	
	^ receiverType = anObject receiver
		and: [ (self arguments hasEqualElements: anObject arguments)
			and: [ self selector = anObject selector
				and: [ self lookupClass = anObject lookupClass ] ] ]
]

{ #category : #accessing }
PIMethodActivation >> arguments [
	
	^ argumentTypes
]

{ #category : #accessing }
PIMethodActivation >> arguments: aCollection [ 
	
	argumentTypes := aCollection
]

{ #category : #testing }
PIMethodActivation >> hasType [
	
	^ message hasTypeForActivation: self
]

{ #category : #comparing }
PIMethodActivation >> hash [

	^ (receiverType hash << 10)	+ self arguments hash
]

{ #category : #lookup }
PIMethodActivation >> lookupClass [

	^ message messageNode isSuperSend
		ifTrue: [ message messageNode methodNode methodClass superclass ]
		ifFalse: [ receiverType concreteClass ].
]

{ #category : #lookup }
PIMethodActivation >> lookupMethod [

	| selector method lookupClass |
	selector := message messageNode selector.
	lookupClass := self lookupClass.
	method := lookupClass lookupSelector: selector.
	method ifNil:[ self error: 'Lookup failed of ' , selector , ' in ' , lookupClass name].
	^ method
]

{ #category : #accessing }
PIMethodActivation >> lookupMethodType [

	^ message typeProvider
		findMethodTypeFor: self lookupMethod ast
		receiver: self receiver
		withArguments: self arguments
		ifAbsent: [ nil ]

]

{ #category : #accessing }
PIMethodActivation >> message [

	^ message
]

{ #category : #accessing }
PIMethodActivation >> message: aPIMessageType [ 
	message := aPIMessageType
]

{ #category : #accessing }
PIMethodActivation >> messageNode [
	
	^ message messageNode
]

{ #category : #accessing }
PIMethodActivation >> methodClass [
	
	^ message messageNode methodNode methodClass
]

{ #category : #accessing }
PIMethodActivation >> methodNode [
	
	^ self lookupMethod ast
]

{ #category : #printing }
PIMethodActivation >> printOn: aStream [
	aStream
		nextPutAll: ' a ';
		nextPutAll: self class name;
		nextPutAll: '(';
		nextPutAll: self receiver printString;
		nextPutAll: ' ';
		nextPutAll: self selector;
		nextPutAll: ')'
]

{ #category : #accessing }
PIMethodActivation >> receiver [
	
	^ receiverType
]

{ #category : #accessing }
PIMethodActivation >> receiver: aReceiverType [
	
	receiverType := aReceiverType
]

{ #category : #accessing }
PIMethodActivation >> returnType [

	^ typeProvider methodTypeForMethodActivation: self
]

{ #category : #accessing }
PIMethodActivation >> returnType: aReturnType [
	^ message addReturnType: aReturnType for: self
]

{ #category : #accessing }
PIMethodActivation >> selector [
	
	^ message messageNode selector
]

{ #category : #accessing }
PIMethodActivation >> typeProvider: aTypeProvider [
	
	typeProvider := aTypeProvider
]
