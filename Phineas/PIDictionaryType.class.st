Class {
	#name : #PIDictionaryType,
	#superclass : #PIAbstractConcreteType,
	#instVars : [
		'associationType'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIDictionaryType >> accept: aVisitor [
	^ aVisitor visitDictionaryType: self
]

{ #category : #accessing }
PIDictionaryType >> associationType: aPIAssociationType [ 
	associationType := aPIAssociationType
]

{ #category : #accessing }
PIDictionaryType >> concreteClass [
	^ Dictionary
]

{ #category : #consolidating }
PIDictionaryType >> consolidateWith: aCollection [
	"A Dictionary type consolidate the key and value types, assuming always there is only one."

	| otherType |
	aCollection
		ifEmpty: [ aCollection add: self.
			^ aCollection ].
	self assert: aCollection size = 1.
	otherType := aCollection anyOne.
	self valueType types do: [ :e | otherType valueType addType: e ].
	self keyType types do: [ :e | otherType keyType addType: e ].
	self becomeForward: otherType.
	^ aCollection
]

{ #category : #variables }
PIDictionaryType >> fillType: typesProvider fromObject: aDictionary [

	"A dictionary is made of an array of associations.
	All nil objects should be excluded from the type."
	aDictionary associationsDo: [ :assoc | 
		associationType valueType addType: (typesProvider typeForObject: assoc value).
		associationType keyType addType: (typesProvider typeForObject: assoc key).		
	]. 
]

{ #category : #testing }
PIDictionaryType >> initialize [ 
	super initialize.
	instanceVariables at: #tally put: (PIVariableType new owner: self ; name: #tally ; yourself).
	instanceVariables at: #array put: (PIVariableType new owner: self ; name: #array ; yourself).
]

{ #category : #testing }
PIDictionaryType >> isDictionaryType [
	^ true
]

{ #category : #accessing }
PIDictionaryType >> keyType [
	^ associationType keyType
]

{ #category : #printing }
PIDictionaryType >> printOnInner: aStream [
	self readable: associationType concreteTypes on: aStream.

	
]

{ #category : #accessing }
PIDictionaryType >> types [
	^{ self keyType. self valueType }
]

{ #category : #accessing }
PIDictionaryType >> valueType [
	^ associationType valueType
]
