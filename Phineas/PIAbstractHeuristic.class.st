Class {
	#name : #PIAbstractHeuristic,
	#superclass : #Object,
	#instVars : [
		'heuristics'
	],
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIAbstractHeuristic >> applyTo: aMethodActivation [

	| inferredType methodType |
	inferredType := self inferTypeOfMethodActivation: aMethodActivation.
	methodType := self typeProvider
		methodTypeForMethodActivation: aMethodActivation
		ifAbsentPut: [ self basicCreateMethodTypeForActivation: aMethodActivation ].
	aMethodActivation returnType: inferredType.
	methodType returnType: inferredType.
	^ inferredType
]

{ #category : #applying }
PIAbstractHeuristic >> applyToMessage: aMessage receiver: receiver arguments: args [
	"If one heuristic's #isApplicableToMessage:receiver:arguments: this message will be called to know what to do with the message"
	self subclassResponsibility
]

{ #category : #'as yet unclassified' }
PIAbstractHeuristic >> basicCreateMethodTypeForActivation: aPIMethodActivation [ 

	^ PIPrimitiveMethodType new
		inferer: self inferer;
		node: aPIMethodActivation methodNode;
		creatingNode: self inferer currentContext;
		receiver: aPIMethodActivation receiver;
		fillArguments: aPIMethodActivation arguments;
		fillTemporaries;
		yourself
]

{ #category : #'special types' }
PIAbstractHeuristic >> handleSpecialTypes: aClass [ 
	^ nil
]

{ #category : #accessing }
PIAbstractHeuristic >> heuristics [
	^ heuristics
]

{ #category : #accessing }
PIAbstractHeuristic >> heuristics: anObject [
	heuristics := anObject
]

{ #category : #applying }
PIAbstractHeuristic >> inferTypeOfMethodActivation: aMethodActivation [

	^ self
		applyToMessage: aMethodActivation messageNode
		receiver: aMethodActivation receiver
		arguments: aMethodActivation arguments
]

{ #category : #accessing }
PIAbstractHeuristic >> inferer [
	^ self heuristics inferer
]

{ #category : #testing }
PIAbstractHeuristic >> isApplicableTo: aMethodActivation [

	^ self
		isApplicableToMessage: aMethodActivation messageNode
		receiver: aMethodActivation receiver
		arguments: aMethodActivation arguments
]

{ #category : #testing }
PIAbstractHeuristic >> isApplicableToMessage: aMessage receiver: aReceiver arguments: arguments [
	"Allows to know if aMessage is treated by an heuristic. Carefull with overlap, only one heuristic will be taken into account
	Returns a Boolean."
	^self subclassResponsibility 
]

{ #category : #accessing }
PIAbstractHeuristic >> typeProvider [
	^ self inferer typeProvider
]
