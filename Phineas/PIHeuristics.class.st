Class {
	#name : #PIHeuristics,
	#superclass : #Object,
	#instVars : [
		'heuristics',
		'inferer'
	],
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #'instance creation' }
PIHeuristics class >> for: aInferer [
	^ self basicNew
		inferer: aInferer;
		initialize;
		yourself
]

{ #category : #adding }
PIHeuristics >> addComplex: aClass [
	heuristics add: (aClass new heuristics: self; yourself).
]

{ #category : #adding }
PIHeuristics >> addInvalidType: selector receiver: receiver args: args [
	self assert: receiver isCollection not.
	self assert: args isCollection.
	self assert: args size = selector numArgs.
	heuristics
		add:
			((PISimpleHeuristic selector: selector receiver: receiver args: args)
				heuristics: self;
				yourself)
]

{ #category : #adding }
PIHeuristics >> addSimple: selector receiver: receiver args: args type: type [
	^ self addSimple: selector receiver: receiver args: args type: type heuristicClass: PISimpleHeuristic
]

{ #category : #adding }
PIHeuristics >> addSimple: selector receiver: receiver args: args type: type heuristicClass: aClass [
	self assert: selector isSymbol.
	self assert: receiver isCollection not.
	self assert: args isCollection.
	self assert: args size = selector numArgs.
	self assert: type isCollection.
	heuristics
		add:
			((aClass
				selector: selector
				receiver: receiver
				args: args
				type: type)
				heuristics: self;
				yourself)
]

{ #category : #adding }
PIHeuristics >> addSmallIntegerHeuristic: selector receiver: receiver args: args type: type [
	^ self addSimple: selector receiver: receiver args: args type: type heuristicClass: PISmallIntegerHeuristic
]

{ #category : #'special types' }
PIHeuristics >> handleSpecialTypes: aClass [
	heuristics do: [ :each | (each handleSpecialTypes: aClass) ifNotNil: [ :aType | ^ aType ] ].
	
	^ inferer typeProvider createConcreteType: aClass
]

{ #category : #accessing }
PIHeuristics >> heuristicFor: aPIMethodActivation ifNone: aBlockClosure [ 
	
	^ heuristics
		detect: [ :heuristic | heuristic isApplicableTo: aPIMethodActivation ]
		ifNone: aBlockClosure
]

{ #category : #accessing }
PIHeuristics >> heuristics [
	^ heuristics
]

{ #category : #accessing }
PIHeuristics >> heuristics: anObject [
	heuristics := anObject
]

{ #category : #accessing }
PIHeuristics >> inferer [
	^ inferer
]

{ #category : #accessing }
PIHeuristics >> inferer: anObject [
	inferer := anObject
]

{ #category : #initialization }
PIHeuristics >> initialize [
	heuristics := OrderedCollection new.
	
	self initializeIlliciumRules.
	
	self addComplex: PIAsHeuristics.
	self initializeSmallIntegerHeuristics.
	self initializeSimpleRules.
	self addComplex: PIBooleanMethodsHeuristic.
	
	self addComplex: PICollectionHeuristics.
	self addComplex: PIDictionaryHeuristics.

	self addComplex: PICaseOfHeuristics.
	
 	self addComplex: PIBasicNewHeuristic.
	self addComplex: PIIfTrueIfFalseHeuristics.
	self addComplex: PIBlockHeuristics.
	self addComplex: PIPerformHeuristics.
	self addComplex: PIObjectAtPutHeuristic.
	self addComplex: PIClassHeuristics.
	self addComplex: PIIfNilHeuristics.
	self addComplex: PIExceptionHeuristics.
	self addComplex: PIFFIHeuristic.
	self addComplex: PISubclassResponsibilityHeuristic.
	self addComplex: PIShouldNotImplementHeuristic.
]

{ #category : #initialization }
PIHeuristics >> initializeIlliciumRules [
	self addSimple: #asString receiver: Object args: {} type: { ByteString }.
	self addSimple: #asInteger receiver: Object args: {} type: { SmallInteger }.
]

{ #category : #initialization }
PIHeuristics >> initializeSimpleRules [

	"SmallInteger class"
	self addSimple: #maxVal receiver: SmallInteger class args: {} type: { SmallInteger }.
	self addSimple: #minVal receiver: SmallInteger class args: {} type: { SmallInteger }.
	
	"Float"
	self addSimple: #+ receiver: Float args: {Float} type: {Float}.
	self addSimple: #+ receiver: Float args: {SmallInteger} type: {Float}.
	self addSimple: #- receiver: Float args: {Float} type: {Float}.
	self addSimple: #- receiver: Float args: {SmallInteger} type: {Float}.
	self addSimple: #* receiver: Float args: {Float} type: {Float}.
	self addSimple: #* receiver: Float args: {SmallInteger} type: {Float}.
	self addSimple: #/ receiver: Float args: {Number} type: {Float}.
	
	"Number"
	self addSimple: #> receiver: Number args: {Number} type: {Boolean}.
	self addSimple: #< receiver: Number args: {Number} type: {Boolean}.
	self addSimple: #>= receiver: Number args: {Number} type: {Boolean}.
	self addSimple: #<= receiver: Number args: {Number} type: {Boolean}.
	self addSimple: #asFloat receiver: Number args: { } type: { Float }.
	self addSimple: #printString receiver: Number args: {} type: { ByteString }.	
	"self addSimple: #asString receiver: Number args: {} type: { ByteString }.	"
	self addSimple: #ln receiver: Number args: { } type: { Float }.
	self addSimple: #log receiver: Number args: { } type: { Float }.
	self addSimple: #log: receiver: Number args: { Number } type: { Float }.


	"Integer"
	self addSimple: #* receiver: Integer args: { Integer } type: { Integer }.
	self addSimple: #+ receiver: Integer args: { Integer } type: { Integer }.
	self addSimple: #- receiver: Integer args: { Integer } type: { Integer }.
	self addSimple: #/ receiver: Integer args: { Integer } type: { Integer. Float }.
	self addSimple: #// receiver: Integer args: { Integer } type: { Integer }.
	self addSimple: #quo: receiver: Integer args: { Integer } type: { Integer }.	
	self addSimple: #bitAnd: receiver: Integer args: { Integer } type: { Integer }.	
	self addSimple: #bitShift: receiver: Integer args: { Integer } type: { Integer }.	
	
	"Object"
	self addSimple: #= receiver: Object args: {Object} type: {Boolean}.
	self addSimple: #== receiver: Object args: {Object} type: {Boolean}.
	self addSimple: #shallowCopy receiver: Object args: {} type: {#self}.
	self addSimple: #hash receiver: Object args: {} type: {SmallInteger}.
	self addSimple: #identityHash receiver: Object args: {} type: {SmallInteger}.
	self addSimple: #basicIdentityHash receiver: Object args: {} type: {SmallInteger}.
	self addSimple: #error: receiver: Object args: {String} type: {#self}.
	self addSimple: #assert: receiver: Object args: {Boolean} type: {#self}.
	self addSimple: #halt receiver: Object args: {} type: {#self}.
	self addSimple: #halt: receiver: Object args: {String} type: {#self}.
	self addSimple: #haltIf: receiver: Object args: {BlockClosure} type: {#self}.
	self addSimple: #basicSize receiver: Object args: {} type: {SmallInteger}.
	
	"Boolean"
	"self addSimple: #and: receiver: Boolean args: {Object} type: {Boolean}.
	self addSimple: #or: receiver: Boolean args: {Object} type: {Boolean}.
	self addSimple: #not receiver: Boolean args: {} type: {Boolean}.
	self addSimple: #| receiver: Boolean args: {Boolean} type: {Boolean}.
	self addSimple: #& receiver: Boolean args: {Boolean} type: {Boolean}.
"	
	"Behaviour"
	self addSimple: #format receiver: Behavior args: {} type: {SmallInteger}.
	
	"String"
	self addSimple: #at: receiver: String args: { Integer } type: { Character }. 
	self addSimple: #at:put: receiver: String args: { Integer. Character } type: { Character }. 
	self addSimple: #byteAt:put: receiver: String args: { SmallInteger. SmallInteger } type: { SmallInteger }. 
	self addSimple: #byteAt:put: receiver: String args: { SmallInteger. Character } type: { SmallInteger }. 
	self addSimple: #byteAt: receiver: String args: { SmallInteger } type: { SmallInteger }. 
	self addSimple: #replaceFrom:to:with:startingAt: receiver: String args:{Integer. Integer. String. Integer} type:{#self}.
	self addSimple: #asSymbol receiver: String args: {} type: { Symbol }.
	self addSimple: #byteSize receiver: String args: {} type: { SmallInteger }.
	self addSimple: #size receiver: String args:{} type:{SmallInteger}.		
	
	"ByteString"
	self addSimple: #basicAt: receiver: ByteString args: { SmallInteger } type: { SmallInteger }.
	self addInvalidType: #at:put: receiver: ByteString args: { SmallInteger. Object }. 	

	"ByteSymbol"
	self addSimple: #basicAt: receiver: ByteSymbol args: { SmallInteger } type: { SmallInteger }.

	"String class"
	self addSimple: #compare:with:collated: receiver: String args: { String. String. ByteArray} type: { SmallInteger }.
	
	"Character"
	"self addSimple: #asInteger receiver: Character args: {} type: {SmallInteger}. "
	self addSimple: #asciiValue receiver: Character args: {} type: {SmallInteger}. 	
		
	"Character class"
	self addSimple: #value: receiver: Character class args: { Integer } type: { Character }.
	
	"HashedCollection class"
	self addSimple: #atLeast: receiver: HashTableSizes class args: {Integer} type: {SmallInteger}. 

	"ByteArray"
	self addSimple: #integerAt:size:signed: receiver: ByteArray args: { SmallInteger. Number. Boolean } type: { SmallInteger }.
	self addSimple: #integerAt:put:size:signed: receiver: ByteArray args: { SmallInteger. Integer. Number. Boolean } type: { SmallInteger }.
	self addSimple: #at: receiver: ByteArray args: { SmallInteger } type: { SmallInteger }. 
	self addSimple: #at:put: receiver: ByteArray args: { SmallInteger. SmallInteger } type: { SmallInteger }. 
	self addSimple: #at:put: receiver: ByteArray args: { SmallInteger. Character } type: { SmallInteger }. 
	self addSimple: #replaceFrom:to:with:startingAt: receiver: ByteArray args:{Integer. Integer. Object. Integer} type:{#self}.
	self addSimple: #size receiver: ByteArray args:{} type:{SmallInteger}.

	self addInvalidType: #at:put: receiver: ByteArray args: { SmallInteger. Object }. 

	"Array"
	self addSimple: #new: receiver: Array class args: { SmallInteger } type: { Array }. 


	"Time class"
	self addSimple: #primUTCMicrosecondsClock receiver: Time class args: {} type: { Integer }.

	"Month class"
	self addSimple: #indexOfMonth: receiver: Month class args: { Object } type: { SmallInteger }.
	
	"VirtualMachine"
	self addSimple: #parameterAt: receiver: VirtualMachine args: { Integer } type: { SmallInteger }.
	self addSimple: #imagePath receiver: VirtualMachine args: { } type: { String }.
	
	"MD5"
	self addSimple: #primProcessBuffer:withState: receiver: MD5 args: { Object. Object }  type: { MD5 }. 
	self addSimple: #hashStream: receiver: MD5  args: { Stream } type: { ByteArray }.
	
	"CompiledCode"
	self addSimple: #numLiterals receiver: CompiledCode args: {} type: { SmallInteger }.
	self addSimple: #trailer receiver: CompiledCode args: {} type: { CompiledMethodTrailer }. 
	self addSimple: #bytecode receiver: CompiledCode args: {} type: { ByteArray }.
	self addSimple: #literalAt: receiver: CompiledCode args: { SmallInteger } type: { Class. SmallInteger. ByteSymbol }.
	self addSimple: #numTemps receiver: CompiledCode args: { } type: { SmallInteger }.
	
	"Interval class"
	self addSimple: #new receiver: Interval class args: { } type: { Interval }.
	
	"Behavior"
	
	self addSimple: #lookupSelector: receiver: Behavior args: { Symbol } type: { CompiledMethod }.	
	self addSimple: #superclass receiver: Behavior args: { } type: { Behavior }.
	self addSimple: #name receiver: Behavior args: {} type: { ByteSymbol }.

]

{ #category : #initialization }
PIHeuristics >> initializeSmallIntegerHeuristics [

	"SmallInteger"
	self addSmallIntegerHeuristic: #+ receiver: SmallInteger args: {SmallInteger} type: {SmallInteger}.
	self addSmallIntegerHeuristic: #+ receiver: SmallInteger args: {Float} type: {Float}.
	self addSmallIntegerHeuristic: #+ receiver: SmallInteger args: {Number} type: {Number}.
	
	self addInvalidType: #+ receiver: SmallInteger args: {Object}.

	self addSmallIntegerHeuristic: #- receiver: SmallInteger args: {SmallInteger} type: {SmallInteger}.
	self addSmallIntegerHeuristic: #- receiver: SmallInteger args: {Float} type: {Float}.
	self addSmallIntegerHeuristic: #* receiver: SmallInteger args: {SmallInteger} type: {SmallInteger}.
	self addSmallIntegerHeuristic: #* receiver: SmallInteger args: {Float} type: {Float}.
	self addSmallIntegerHeuristic: #// receiver: SmallInteger args: {SmallInteger} type: {SmallInteger}.
	self addSmallIntegerHeuristic: #\\ receiver: SmallInteger args: {SmallInteger} type: {SmallInteger}.
	self addSmallIntegerHeuristic: #/ receiver: SmallInteger args: {SmallInteger} type: {SmallInteger}.
	self addSmallIntegerHeuristic: #quo: receiver: SmallInteger args: { SmallInteger } type: { SmallInteger }.
	self addSmallIntegerHeuristic: #raisedTo: receiver: SmallInteger args: { SmallInteger } type: { SmallInteger }.


	self addSmallIntegerHeuristic: #~= receiver: SmallInteger args: { SmallInteger } type: { Boolean }.
	self addSmallIntegerHeuristic: #= receiver: SmallInteger args: {SmallInteger} type: {Boolean}.
	self addSmallIntegerHeuristic: #> receiver: SmallInteger args: {SmallInteger} type: {Boolean}.
	self addSmallIntegerHeuristic: #< receiver: SmallInteger args: {SmallInteger} type: {Boolean}.
	self addSmallIntegerHeuristic: #>= receiver: SmallInteger args: {SmallInteger} type: {Boolean}.
	self addSmallIntegerHeuristic: #<= receiver: SmallInteger args: {SmallInteger} type: {Boolean}.

	self addSmallIntegerHeuristic: #bitInvert32 receiver: SmallInteger args: {}  type: { SmallInteger }. 
	self addSmallIntegerHeuristic: #bitShift: receiver: SmallInteger args: {SmallInteger} type: {SmallInteger}.
	self addSmallIntegerHeuristic: #bitAnd: receiver: SmallInteger args: {SmallInteger} type: {SmallInteger}.
	self addSmallIntegerHeuristic: #asCharacter receiver: SmallInteger args: {} type: { Character }.

]

{ #category : #querying }
PIHeuristics >> translateAlias: aClass [
	(aClass inheritsFrom: Float)
		ifTrue: [ ^ Float ].
	
	(aClass inheritsFrom: Boolean)
		ifTrue: [ ^ Boolean ].
		
	(aClass inheritsFrom: String)
		ifTrue:[ ^ ByteString ].
	^ aClass
]
