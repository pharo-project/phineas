Class {
	#name : #PIGlobalEnvironment,
	#superclass : #Object,
	#category : #'Phineas-Contexts'
}

{ #category : #accessing }
PIGlobalEnvironment >> at: aKey [
	^ Smalltalk globals at: aKey
]

{ #category : #accessing }
PIGlobalEnvironment >> at: aString ifPresent: aBlockClosure ifAbsent: aBlockClosure3 [
	^ Smalltalk globals at: aString ifPresent: aBlockClosure ifAbsent: aBlockClosure3
]

{ #category : #extending }
PIGlobalEnvironment >> extendWith: aCollection [
	^ PIEnvironment new
		parentEnvironment: self;
		initializeWithBindings: aCollection;
		yourself
]
