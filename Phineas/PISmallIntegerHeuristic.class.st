Class {
	#name : #PISmallIntegerHeuristic,
	#superclass : #PISimpleHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PISmallIntegerHeuristic >> basicCreateMethodTypeForActivation: aPIMethodActivation [ 

	^ PISmallIntegerMethodType new
		node: aPIMethodActivation methodNode;
		creatingNode: self inferer currentContext;
		receiver: aPIMethodActivation receiver;
		inferer: self inferer;
		fillArguments: aPIMethodActivation arguments;
		fillTemporaries;
		yourself
]
