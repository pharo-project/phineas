Class {
	#name : #PIAbstractTypeHeuristic,
	#superclass : #Object,
	#category : #'Phineas-Heuristics-ExplicitTypes'
}

{ #category : #'explicit-type' }
PIAbstractTypeHeuristic >> at: aSymbol [
	"returns the class corresponding to the pragma value"

	self subclassResponsibility
]

{ #category : #'error handling' }
PIAbstractTypeHeuristic >> error: aType [
	^ UnknownExplicitType new explicitType: aType ; signal
]

{ #category : #variables }
PIAbstractTypeHeuristic >> returnTypeSelectors [
	"must return an collection of authorized pragmas"

	self subclassResponsibility
]

{ #category : #variables }
PIAbstractTypeHeuristic >> variableSelectors [
	"must return an collection of authorized pragmas"

	self subclassResponsibility
]
