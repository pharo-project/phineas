Class {
	#name : #PIVariableType,
	#superclass : #Object,
	#instVars : [
		'receivedMessages',
		'owner',
		'name',
		'wasQueried',
		'requestingNodes',
		'types'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIVariableType >> accept: aVisitor [
	^ aVisitor visitVariableType: self
]

{ #category : #adding }
PIVariableType >> addReceivedMessage: aMessageType [
	receivedMessages add: aMessageType
]

{ #category : #adding }
PIVariableType >> addRequestingNodes: aMethodType [
	requestingNodes add: aMethodType
]

{ #category : #evolution }
PIVariableType >> addType: anotherType [
	self types add: anotherType.
	self consolidateTypes
]

{ #category : #evolution }
PIVariableType >> addTypes: someTypes [
	someTypes do: [ :aType| self addType: aType ]
]

{ #category : #converting }
PIVariableType >> asExplicitType: aType [
	^PIExplicitType new
		types: types;
		receivedMessages: receivedMessages;
		owner: owner;
		explicitType: aType;
		wasQueried: wasQueried ;
		requestingNodes: requestingNodes;
		yourself.
]

{ #category : #converting }
PIVariableType >> becomeExplicit: aType [ 
	self becomeForward: (self asExplicitType: aType)
]

{ #category : #converting }
PIVariableType >> becomeInvalid: aType [ 
	self becomeForward: (self asInvalidType: aType).
]

{ #category : #consolidating }
PIVariableType >> browse [
	owner browse
]

{ #category : #accessing }
PIVariableType >> concreteClass [
	^ self concreteType concreteClass
]

{ #category : #accessing }
PIVariableType >> concreteClasses [
	^ self concreteTypes flatCollect: [ :e | e concreteClasses ]
]

{ #category : #accessing }
PIVariableType >> concreteType [
	
	| concreteTypes |
	concreteTypes := self concreteTypes.
	self assert: concreteTypes size = 1  description: '#concreteType is only usable when there is only one concreteType'.
	
	^ concreteTypes anyOne
]

{ #category : #accessing }
PIVariableType >> concreteTypeVisited: visited receivedMessages: allReceived [
	(visited includes: self)
		ifTrue: [ ^ #() ].
	visited add: self.

	^ types
		flatCollect: [ :e | e concreteTypeVisited: visited receivedMessages: allReceived, receivedMessages ].
]

{ #category : #accessing }
PIVariableType >> concreteTypes [
	wasQueried:= true.
	^ self concreteTypeVisited: IdentitySet new receivedMessages: receivedMessages
]

{ #category : #consolidating }
PIVariableType >> consolidateTypes [
	"This methods flattens the types by kind. Only should be used when initializing concrete types from objects"

	"I produce all the concreteTypes but I consolidate them to reduce the number of possibilities."

	| concreteTypes grouped consolidated |
	concreteTypes := self
		concreteTypeVisited: IdentitySet new
		receivedMessages: receivedMessages.
	grouped := concreteTypes groupedBy: [ :e | e class ].

	consolidated := Set new.
	grouped
		valuesDo: [ :grp | 
			consolidated
				addAll:
					(grp
						inject: OrderedCollection new
						into: [ :accum :e | e consolidateWith: accum ]) ]
]

{ #category : #accessing }
PIVariableType >> elementType [
	^ self class new
		types: (types collect: #elementType);
		yourself
]

{ #category : #flattening }
PIVariableType >> flatTypes: aValuable visited: visited [
	
	(visited includes: self) ifTrue: [ ^ #() ].
	
	visited add: self.
]

{ #category : #testing }
PIVariableType >> ifEmpty: aBlock [
	^ types ifEmpty: aBlock
]

{ #category : #testing }
PIVariableType >> includes: aType [
	
	self = aType ifTrue: [ ^ true ].
	^ types anySatisfy: [ :any | any includes: aType ]
]

{ #category : #initialization }
PIVariableType >> initialize [
	super initialize.
	wasQueried := false.
	types := Set new.
	receivedMessages := Set new.
	requestingNodes := OrderedCollection new.
]

{ #category : #testing }
PIVariableType >> isBlockType [
	^ false
]

{ #category : #accessing }
PIVariableType >> isEmpty [
	
	^ self concreteTypes isEmpty
]

{ #category : #testing }
PIVariableType >> isExplicit [ 
	
	^ false
]

{ #category : #testing }
PIVariableType >> isMessageType [ 
	^ false
]

{ #category : #testing }
PIVariableType >> isValid [
	^ true
]

{ #category : #testing }
PIVariableType >> isVariableType [
	^ true
]

{ #category : #accessing }
PIVariableType >> name [
	^ name
]

{ #category : #accessing }
PIVariableType >> name: aString [
	name := aString
]

{ #category : #accessing }
PIVariableType >> needProcessing [
	^ wasQueried and: [ self concreteTypes isEmpty ]
]

{ #category : #accessing }
PIVariableType >> owner [
	^ owner
]

{ #category : #accessing }
PIVariableType >> owner: anImmutableType [
	owner := anImmutableType
]

{ #category : #printing }
PIVariableType >> printOn: aStream [

	self printOn: aStream visited: IdentitySet new.
]

{ #category : #printing }
PIVariableType >> printOn: aStream visited: visited [	
	| oldWasQueried |
	oldWasQueried := wasQueried.
	aStream
		nextPutAll: 'Var';
		nextPutAll: ':(';
		print: name ;
		nextPutAll:' -> '.
	self concreteTypes ifNotNil: [:cts| self readable: cts on: aStream ].
		
	wasQueried := oldWasQueried.
"	(visited includes: self) ifFalse: [  
		visited add: self.
		types do: [ :t | 
			t printOn: aStream visited: visited.
			aStream nextPut: Character space ].	
	] ifTrue: [  
		aStream nextPutAll: '...'
	]."

	aStream nextPut: $)
]

{ #category : #testing }
PIVariableType >> readable: concreteTypes on: aStream [
	concreteTypes size = 1
		ifTrue: [ concreteTypes anyOne printOnInner: aStream.
			^ self ].
	aStream << '{'.
	concreteTypes
		do: [ :aConcreteType | aConcreteType printOnInner: aStream ]
		separatedBy: [ aStream << ' , ' ].
	aStream << '}'
]

{ #category : #testing }
PIVariableType >> readableConcreteTypesOn: aStream [
	aStream << '{'.
	self concreteTypes
		do: [ :aConcreteType | aConcreteType printOn: aStream ]
		separatedBy: [ aStream << ' , ' ].
	aStream << '}'
]

{ #category : #accessing }
PIVariableType >> receivedMessages [
	^ receivedMessages 
]

{ #category : #accessing }
PIVariableType >> receivedMessages: aCollection [
	receivedMessages := aCollection
]

{ #category : #accessing }
PIVariableType >> requestingNodes [
	^ requestingNodes 
]

{ #category : #accessing }
PIVariableType >> requestingNodes: aCollection [
	requestingNodes := aCollection
]

{ #category : #accessing }
PIVariableType >> types [
	^ types
]

{ #category : #accessing }
PIVariableType >> types: anObject [
	types := anObject
]

{ #category : #accessing }
PIVariableType >> wasQueried [
	^ wasQueried
]

{ #category : #accessing }
PIVariableType >> wasQueried: aBoolean [
	wasQueried := aBoolean
]

{ #category : #accessing }
PIVariableType >> wdttcf [
"	owner inferer typeProvider messageTypes select:[:aMethod| (aMethod concreteClasses includes: String) and: [ self includes: aMethod ] ] "
	PITypeInspector new
    root: self;
    openWithSpec;
    yourself.
]
