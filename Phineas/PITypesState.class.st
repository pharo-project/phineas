Class {
	#name : #PITypesState,
	#superclass : #Object,
	#instVars : [
		'concreteTypes',
		'blockTypes',
		'methodTypes',
		'symbolTypes',
		'inferer',
		'objectTypes',
		'messageTypes',
		'invalidType',
		'explicitTypeHeuristic'
	],
	#category : #Phineas
}

{ #category : #accessing }
PITypesState >> addMethodType: aPIMethodType [ 
	methodTypes add: aPIMethodType.
]

{ #category : #accessing }
PITypesState >> addType: aPIMethodType [ 
	methodTypes add: aPIMethodType.
]

{ #category : #types }
PITypesState >> blockTypeFor: aBlockNode [
	^ blockTypes detect:[:e | e isFor: aBlockNode in: inferer currentContext ] ifNone: [ 
		blockTypes add: (self doBlockTypeFor: aBlockNode)
	]
 
	
]

{ #category : #accessing }
PITypesState >> blockTypes [
	^ blockTypes
]

{ #category : #'special types' }
PITypesState >> concreteTypes [
	^ concreteTypes
]

{ #category : #'special types' }
PITypesState >> createAssociationType [
	^ PIAssociationType new
		creatingNode: inferer currentContext;
		yourself
]

{ #category : #'special types' }
PITypesState >> createCollectionType: aClass [
	^ PICollectionType new
		collectionType: (self createConcreteType: aClass);
		creatingNode: inferer currentContext;
		yourself
]

{ #category : #types }
PITypesState >> createConcreteType: aClass [
	^ concreteTypes at: aClass name ifAbsent: [ self doCreateConcreteType: aClass ]
]

{ #category : #'special types' }
PITypesState >> createDictionaryType [

	^ PIDictionaryType new
		associationType: self createAssociationType;
		creatingNode: inferer currentContext;
		yourself
]

{ #category : #private }
PITypesState >> doBlockTypeFor: aBlockNode [
	self assert: (inferer currentContext isRootContext not).

	^ PIBlockType new
		node: aBlockNode copy;
		homeContext: inferer currentContext;
		inferer: inferer;
		creatingNode: inferer currentContext;
		fillTemporaries;
		yourself
]

{ #category : #types }
PITypesState >> doCreateConcreteType: aClass [
	| aNewConcreteType concreteTypeClass|
	
	concreteTypeClass  := aClass isMeta ifTrue: [ PIConcreteMetaType ] ifFalse: [ PIConcreteType ].

	aNewConcreteType := concreteTypeClass new
		concreteClass: aClass;
		creatingNode: inferer currentContext;
		yourself.

	concreteTypes at: aClass name put: aNewConcreteType.
	aClass superclass ifNotNil: [ :s | aNewConcreteType superclass: (self typeForClass: s) ].
	aNewConcreteType fillType: self.
	
	^ aNewConcreteType
]

{ #category : #private }
PITypesState >> doCreateSymbolType: aSymbol [
	^ PISymbolType new
		value: aSymbol;
		concreteType: (self createConcreteType: aSymbol class);
		creatingNode: inferer currentContext;
		yourself
]

{ #category : #'defined-types' }
PITypesState >> emptyType [
	^ PIVariableType new
]

{ #category : #accessing }
PITypesState >> explicitTypeHeuristic [
	^ explicitTypeHeuristic
]

{ #category : #accessing }
PITypesState >> explicitTypeHeuristic: anExplicitTypeHeuristic [
	explicitTypeHeuristic := anExplicitTypeHeuristic
]

{ #category : #types }
PITypesState >> findMethodTypeFor: aMethodNode receiver: aReceiver withArguments: args ifAbsent: aBlockClosure [

	^ methodTypes
		detect: [ :e | e isForMethodNode: aMethodNode receiver: aReceiver with: args ]
		ifNone: aBlockClosure.
]

{ #category : #accessing }
PITypesState >> inferer [
	^ inferer
]

{ #category : #accessing }
PITypesState >> inferer: anObject [
	inferer := anObject
]

{ #category : #initialization }
PITypesState >> initialize [
	super initialize.

	concreteTypes := Dictionary new.
	objectTypes := IdentityDictionary new.
	methodTypes := OrderedCollection new.
	blockTypes := OrderedCollection new.
	symbolTypes := Dictionary new.
	messageTypes := OrderedCollection new.
	
	invalidType := PIInvalidType new.
	
	explicitTypeHeuristic := PISmalltalkTypesHeuristic.
]

{ #category : #'defined-types' }
PITypesState >> invalidType [
	^ invalidType
]

{ #category : #accessing }
PITypesState >> messageTypes [
	^ messageTypes
]

{ #category : #querying }
PITypesState >> methodTypeForMethodActivation: aPIMethodActivation [ 
	
	^ self
		methodTypeForMethodActivation: aPIMethodActivation
		ifAbsent: [ NotFound signalFor: aPIMethodActivation ]
]

{ #category : #querying }
PITypesState >> methodTypeForMethodActivation: aPIMethodActivation ifAbsent: aBlockClosure [ 

	^ methodTypes
		detect: [ :e | e
				isForMethodNode: aPIMethodActivation methodNode
				receiver: aPIMethodActivation receiver
				with: aPIMethodActivation arguments ]
		ifNone: aBlockClosure
]

{ #category : #querying }
PITypesState >> methodTypeForMethodActivation: aPIMethodActivation ifAbsentPut: aBlockClosure [ 

	^ self
		methodTypeForMethodActivation: aPIMethodActivation
		ifAbsent: [ | methodType |
			methodType := aBlockClosure value.
			self addType: methodType.
			methodType ]
]

{ #category : #methods }
PITypesState >> methodTypes [
	^ methodTypes
]

{ #category : #'defined-types' }
PITypesState >> nilType [
	^ self typeForClass: UndefinedObject
]

{ #category : #types }
PITypesState >> symbolTypeFor: aSymbol [
	^ symbolTypes at: aSymbol ifAbsentPut: [ self doCreateSymbolType: aSymbol ]
 
	
]

{ #category : #types }
PITypesState >> typeForClass: aClassToAlias [
	| aClass |
		
	aClass := inferer heuristics translateAlias: aClassToAlias.
	self assert: aClass isClass.

	^ self inferer heuristics handleSpecialTypes: aClass

]

{ #category : #types }
PITypesState >> typeForLiteral: aLiteral [
	(aLiteral class includesBehavior: Symbol)
		ifTrue:[^ self symbolTypeFor: aLiteral].
		 
	^ self typeForClass: aLiteral class.
]

{ #category : #types }
PITypesState >> typeForMessage: aMessageAST receiver: receiverType arguments: argumentTypes homeContext: homeContext [
	| msg |
	messageTypes
		detect: [ :e | e message = aMessageAST and: [ e receiver = receiverType and: [ (e arguments hasEqualElements: argumentTypes) and: [ e homeContext = homeContext ]]]]
		ifFound: [ :x | ^ x ].

	msg := PIMessageType new
		typeProvider: self;
		message: aMessageAST;
		receiver: receiverType;
		arguments: argumentTypes;
		homeContext: homeContext;
		yourself.

	receiverType addReceivedMessage: msg.
	messageTypes add: msg.

	^ msg
]

{ #category : #types }
PITypesState >> typeForObject: anObject [ 
	
	| typeOfClass |
	"If it is a class we handle it as a class"
	anObject isClass 
		ifTrue: [ ^ self typeForClass: anObject  ].

	
	objectTypes at: anObject ifPresent: [ :type | ^ type ].
	typeOfClass := self typeForClass: anObject class.
	objectTypes at: anObject put: typeOfClass.
	typeOfClass fillType: self fromObject: anObject.
	^ typeOfClass
]

{ #category : #querying }
PITypesState >> typesOfMethod: aSelector from: aClass [

	^ methodTypes select: [ :e | (e node selector = aSelector) and: [ e node methodClass = aClass ] ]
]

{ #category : #types }
PITypesState >> variableType: aCollection [
	^ PIVariableType new
		types: aCollection asSet;
		yourself
]

{ #category : #types }
PITypesState >> variableTypeFromClasses: aCollection [
	^ self variableType: (aCollection asSet collect: [ :aClass | self typeForClass: aClass ])
]
