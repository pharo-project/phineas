Class {
	#name : #PIBlockHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIBlockHeuristics >> applyToMessage: aMessage receiver: aReceiver arguments: args [ 
	
	(aMessage selector = #on:do:) ifTrue: [ ^ self handleOnDo: aMessage receiver: aReceiver arguments: args ].
	
	aReceiver calculateTypesWith: args.
	^ aReceiver returnType
]

{ #category : #applying }
PIBlockHeuristics >> handleOnDo: aMessage receiver: aReceiver arguments: args [

	aReceiver calculateTypes.
	args second calculateTypesWith: {args first baseType}.
	^ self inferer variableType: { aReceiver returnType. args second returnType }
]

{ #category : #testing }
PIBlockHeuristics >> isApplicableToMessage: aMessage receiver: receiver arguments: args [
	^ receiver isBlockType
		and: [ #(
			value 
			value: 
			value:value:
			value:value:value:
			value:value:value:value: 
			cull: 
			cull:cull:
			cull:cull:cull:
			cull:cull:cull:cull:
			on:do:) includes: aMessage selector ]
]
