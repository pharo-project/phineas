Class {
	#name : #PIAbstractConcreteType,
	#superclass : #PIImmutableType,
	#instVars : [
		'superclassType',
		'concreteClass',
		'instanceVariables',
		'classVariables',
		'poolDictionaries'
	],
	#category : #'Phineas-Types'
}

{ #category : #variables }
PIAbstractConcreteType >> browse [
	concreteClass browse
]

{ #category : #variables }
PIAbstractConcreteType >> classVariable: aName ifPresent: aBlock [
	classVariables at: aName ifPresent: [ :v | ^ aBlock cull: v ].
	superclassType ifNotNil: [ superclassType classVariable: aName ifPresent: aBlock ].
	poolDictionaries do: [ :e | e classVariable: aName ifPresent: aBlock ].
]

{ #category : #variables }
PIAbstractConcreteType >> classVariable: aName ifPresent: aBlock ifAbsent:anAbsentBlock [
	classVariables at: aName ifPresent: [ :v | ^ aBlock cull: v ].
	superclassType ifNotNil: [ superclassType classVariable: aName ifPresent: aBlock ].
	poolDictionaries do: [ :e | e classVariable: aName ifPresent: aBlock ].
	anAbsentBlock value.
]

{ #category : #variables }
PIAbstractConcreteType >> classVariables [
	^ classVariables 
]

{ #category : #accessing }
PIAbstractConcreteType >> concreteClass: anObject [
	concreteClass := anObject.	

]

{ #category : #accessing }
PIAbstractConcreteType >> concreteType [
	
	^ self
]

{ #category : #'initialization-release' }
PIAbstractConcreteType >> consolidateTypes [
	instanceVariables valuesDo: #consolidateTypes.
	classVariables valuesDo: #consolidateTypes.	
]

{ #category : #initialization }
PIAbstractConcreteType >> initialize [
	super initialize.
	instanceVariables := Dictionary new.
	classVariables := Dictionary new.
	poolDictionaries := OrderedCollection new.
]

{ #category : #variables }
PIAbstractConcreteType >> instanceVariable: aName [
	^ (self instanceVariable: aName ifAbsent: [ NotFound signalFor: aName ] ) value
]

{ #category : #variables }
PIAbstractConcreteType >> instanceVariable: aName ifAbsent: anAbsentBlock [
	^ self instanceVariable: aName ifPresent: [:anAssociation| ^ anAssociation value ] ifAbsent: anAbsentBlock 
]

{ #category : #variables }
PIAbstractConcreteType >> instanceVariable: aName ifPresent: aBlock [
	^ self instanceVariable: aName ifPresent: aBlock ifAbsent: [ ]
]

{ #category : #variables }
PIAbstractConcreteType >> instanceVariable: aName ifPresent: aPresentBlock ifAbsent: anAbsentBlock [
	^ instanceVariables 
		at: aName
		ifPresent: aPresentBlock	
		ifAbsent: anAbsentBlock
]

{ #category : #accessing }
PIAbstractConcreteType >> instanceVariables [
	^ instanceVariables
]

{ #category : #accessing }
PIAbstractConcreteType >> name [
	^ self concreteClass name
]

{ #category : #accessing }
PIAbstractConcreteType >> needProcessing [
	^ instanceVariables anySatisfy: #needProcessing
]

{ #category : #accessing }
PIAbstractConcreteType >> poolDictionaries [
	^ poolDictionaries
]

{ #category : #accessing }
PIAbstractConcreteType >> superclass [
	^ superclassType
]

{ #category : #accessing }
PIAbstractConcreteType >> superclass: aValue [
	superclassType := aValue
]

{ #category : #variables }
PIAbstractConcreteType >> variableAt: aName [
	^ self variableAt: aName ifAbsent: [ KeyNotFound signal ] 
]

{ #category : #variables }
PIAbstractConcreteType >> variableAt: aName ifAbsent: anAbsentBlock [
	self classVariable: aName ifPresent: [ :v | ^ v ].
	^ self
		instanceVariable: aName
		ifPresent: [ :v | v ]
		ifAbsent: [ superclassType
				ifNotNil: [ superclassType variableAt: aName ifAbsent: anAbsentBlock ]
				ifNil: anAbsentBlock ]
]

{ #category : #variables }
PIAbstractConcreteType >> variableAt: aName ifPresent: aPresentBlock ifAbsent: anAbsentBlock [
	self classVariable: aName ifPresent: aPresentBlock.
	^ self
		instanceVariable: aName
		ifPresent: aPresentBlock
		ifAbsent: [ superclassType
				ifNotNil: [ superclassType variableAt: aName ifPresent: aPresentBlock ifAbsent: anAbsentBlock ]
				ifNil: anAbsentBlock ]
]

{ #category : #accessing }
PIAbstractConcreteType >> variablesNeedingProcessing [
	^ instanceVariables select:#needProcessing thenCollect:#value
]
