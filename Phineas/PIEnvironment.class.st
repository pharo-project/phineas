Class {
	#name : #PIEnvironment,
	#superclass : #Object,
	#instVars : [
		'parentEnvironment',
		'bindings'
	],
	#category : #'Phineas-Contexts'
}

{ #category : #accessing }
PIEnvironment >> at: aKey [

	^ bindings at: aKey ifAbsent: [ parentEnvironment at: aKey ]
]

{ #category : #accessing }
PIEnvironment >> at: aKey ifPresent: presentBlock ifAbsent: absentBlock [
	
	^ bindings
		at: aKey
		ifPresent: presentBlock
		ifAbsent: [ parentEnvironment at: aKey ifPresent: presentBlock ifAbsent: absentBlock ]
]

{ #category : #extending }
PIEnvironment >> extendWith: aCollection [
	^ PIEnvironment new
		parentEnvironment: self;
		initializeWithBindings: aCollection;
		yourself
]

{ #category : #'initialize-release' }
PIEnvironment >> initializeWithBindings: aCollection [ 
	bindings := aCollection asDictionary
]

{ #category : #accessing }
PIEnvironment >> parentEnvironment: aPIEnvironment [ 
	parentEnvironment := aPIEnvironment
]

{ #category : #iterating }
PIEnvironment >> valuesDo: aBlockClosure [ 
	
	bindings do: aBlockClosure
]
