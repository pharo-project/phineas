Class {
	#name : #PISmalltalkMethodType,
	#superclass : #PIMethodType,
	#instVars : [
		'explicitTypeHeuristic',
		'explicitBindings'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PISmalltalkMethodType >> accept: aVisitor [

	^ aVisitor visitSmalltalkMethodType: self
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> at: aSymbol [
	^ ((explicitTypeHeuristic at: aSymbol) asPhineasType: self inferer typeProvider)
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> explicitReturnType: aPragma [
	returnType becomeExplicit: (self at: aPragma arguments first value).
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> explicitTypeForAVariable: aPragma [
	| type identifier |
	type := self at: aPragma arguments second value.
	identifier := aPragma arguments first value.
	type
		ifNil: [ ^ identifier -> (PIVariableType new owner: self ; name: identifier ; yourself) ]
		ifNotNil: [ 
			^ identifier -> (PIExplicitType new 
				owner: self ;
				name: identifier ;
				explicitType: type;
				yourself ) ]
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> explicitTypeHeuristic [
	^ explicitTypeHeuristic
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> fillArguments: values [
	^ super
		fillArguments:
			(values
				ifEmpty: [ self setArguments asArray ]
				ifNotEmpty: [ :size | (self setArguments: values) asArray ])
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> fillTemporaries [
	node temporaryNames
		do: [ :tempName | 
			| association |
			association := explicitBindings associationAt: tempName ifAbsent: nil.
			association
				ifNotNil: [ variables add: association ]
				ifNil: [ variables at: tempName put: (PIVariableType new owner: self ; name: tempName; yourself)] ]
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> getPragmasInformations [
	explicitBindings := Dictionary new.
	node pragmas
		do: [ :aPragma | 
			(self explicitTypeHeuristic variableSelectors
				includes: aPragma selector)
				ifTrue: [ explicitBindings add: (self explicitTypeForAVariable: aPragma) ].
			(self explicitTypeHeuristic returnTypeSelectors
				includes: aPragma selector)
				ifTrue: [ self explicitReturnType: aPragma ] ].
	
	"try to type IVs of the receiver using type annotations"
	explicitBindings associationsDo:[:association |
		receiver variableAt: association key
			ifPresent:[:var | var becomeExplicit: association value concreteType ]
			ifAbsent: nil ]
]

{ #category : #accessing }
PISmalltalkMethodType >> inferer: anInferer [
	super inferer: anInferer.
	explicitTypeHeuristic := inferer explicitTypeHeuristic new
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> setArguments [
	^ self node arguments
		collect: [ :anArg | 
			explicitBindings
				at: anArg name
				ifPresent: [ :aValue | aValue ]
				ifAbsent: [ UnresolvedBinding new unboundVariable: anArg ; signal  ] ]
		into: (Array new: self node arguments size)
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> setArguments: values [
	| counter |
	counter := 0.
	

	^ self node arguments
		collect: [ :anArg | 
			counter := counter + 1.
			(explicitBindings at: anArg name ifAbsent: nil)
				ifNotNil: [ :aValue | aValue ]
				ifNil: [ values at: counter ] ]
		into: (Array new: self node arguments size )
]

{ #category : #'explicit-type' }
PISmalltalkMethodType >> trySetReturnType [
	returnType isExplicit
		ifTrue: [ ^ self ].
	node hasReturn
		ifFalse: [ returnType
				addType: (inferer typeProvider createConcreteType: node methodClass) ]
]
