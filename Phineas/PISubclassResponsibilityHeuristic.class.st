Class {
	#name : #PISubclassResponsibilityHeuristic,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PISubclassResponsibilityHeuristic >> applyToMessage: aMessage receiver: aReceiver arguments: args [
	self error: 'subclassResponsibility messages send aren''t taken care off yet.'
]

{ #category : #testing }
PISubclassResponsibilityHeuristic >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [
	^ aMessage selector = #subclassResponsibility
]
