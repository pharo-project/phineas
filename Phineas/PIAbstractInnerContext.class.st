Class {
	#name : #PIAbstractInnerContext,
	#superclass : #PIAbstractContext,
	#instVars : [
		'parent',
		'type',
		'sender'
	],
	#category : #'Phineas-Contexts'
}

{ #category : #testing }
PIAbstractInnerContext >> isRootContext [
	^ false
]

{ #category : #accessing }
PIAbstractInnerContext >> parent [
	^ parent
]

{ #category : #accessing }
PIAbstractInnerContext >> parent: anObject [
	parent := anObject
]

{ #category : #initialization }
PIAbstractInnerContext >> receiver [
	^ self subclassResponsibility
]

{ #category : #accessing }
PIAbstractInnerContext >> sender [
	^ sender
]

{ #category : #accessing }
PIAbstractInnerContext >> sender: anObject [
	sender := anObject
]

{ #category : #accessing }
PIAbstractInnerContext >> type [
	^ type
]

{ #category : #accessing }
PIAbstractInnerContext >> type: anObject [
	type := anObject
]
