Class {
	#name : #PIBooleanMethodsHeuristic,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIBooleanMethodsHeuristic >> applyToMessage: aMessage receiver: receiver arguments: args [
	(args size = 1 and: [ aMessage isKeyword ])
		ifTrue: [ args anyOne calculateTypes ].
	self inferer
		inferMessage: aMessage
		receiver: receiver
		arguments: args.
	^ self typeProvider typeForClass: Boolean
]

{ #category : #testing }
PIBooleanMethodsHeuristic >> isApplicableToMessage: aMessage receiver: aReceiver arguments: arguments [
	^ (aReceiver isOfType: Boolean) and:[ #(#and: #or: #xor: not & |) includes: aMessage selector ]
]
