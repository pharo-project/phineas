Class {
	#name : #PIIfTrueIfFalseMethodType,
	#superclass : #PIMethodType,
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIIfTrueIfFalseMethodType >> accept: aVisitor [ 

	^ aVisitor visitIfTrueIfFalseMethodType: self
]
