Class {
	#name : #PIPragmaHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIPragmaHeuristics >> applyToMessage: aMessage receiver: aReceiver arguments: arguments [ 
	| aMethod returnTypeName returnTypeClass calculatedType |
	
	aMethod := self lookupMethodForMessage: aMessage in: aReceiver ifAbsent: [ self error: 'Failing doing the lookup of ' , aMessage ].
	
	returnTypeName := (aMethod pragmaAt: #returnType:) arguments first.
	calculatedType := self inferer inferMessage: aMessage receiver: aReceiver arguments: arguments.
	returnTypeClass := self inferer explicitTypeHeuristic new at: returnTypeName.
	
	^ self typeProvider typeForClass: returnTypeClass
]

{ #category : #testing }
PIPragmaHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [
	| aMethod |

	aMethod := self lookupMethodForMessage: aMessage in: aReceiver ifAbsent: [ ^ false ].
	
	^ aMethod hasPragmaNamed: #returnType:
	
]

{ #category : #lookup }
PIPragmaHeuristics >> lookupMethodForMessage: aMessage in: aReceiver ifAbsent: aBlockClosure [ 
	| lookupClass |
	
	lookupClass := aMessage isSuperSend
		ifTrue: [ aMessage methodNode methodClass superclass ]
		ifFalse: [ aReceiver concreteClass ].
		
	^ lookupClass methodDict at: aMessage selector ifAbsent: aBlockClosure.
]
