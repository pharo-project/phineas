Class {
	#name : #UnknownExplicitType,
	#superclass : #Error,
	#instVars : [
		'explicitType'
	],
	#category : #'Phineas-Errors'
}

{ #category : #accessing }
UnknownExplicitType >> description [
	^ String streamContents: [ :s|
		s << 'Unknown explicit type '
			print: explicitType
		]
]

{ #category : #accessing }
UnknownExplicitType >> explicitType [
	^ explicitType
]

{ #category : #accessing }
UnknownExplicitType >> explicitType: aType [
	explicitType := aType
]
