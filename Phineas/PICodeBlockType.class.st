Class {
	#name : #PICodeBlockType,
	#superclass : #PIImmutableType,
	#instVars : [
		'node',
		'receiver',
		'inferer',
		'variables',
		'returnType',
		'messagesTypes'
	],
	#category : #'Phineas-Types'
}

{ #category : #variables }
PICodeBlockType >> addMessageType: aMessageType [
	messagesTypes add: aMessageType
]

{ #category : #variables }
PICodeBlockType >> arguments [
	^ node arguments collect: [ :e | variables at: e name ]
]

{ #category : #accessing }
PICodeBlockType >> browse [
	self node methodNode method browse
]

{ #category : #variables }
PICodeBlockType >> fillArguments: values [
	node arguments size > values size ifTrue:[
		UnresolvedBinding new  signal: 'Can''t infer a method''s AST with unset arguments'.
		].
	
	node arguments with: (values first: node arguments size) do:[ :arg :value |
		variables at: arg name put: value ]
]

{ #category : #variables }
PICodeBlockType >> fillTemporaries [ 
	node temporaryNames do:[ :tempName |
		variables at: tempName put: ( PIVariableType new owner: self ; name: tempName ; yourself )	
	]
]

{ #category : #accessing }
PICodeBlockType >> getMessageType: aRBMessageNode [
	^ messagesTypes detect: [:aMessageType| aMessageType message = aRBMessageNode ]
]

{ #category : #accessing }
PICodeBlockType >> homeContext [
	^ self subclassResponsibility
]

{ #category : #accessing }
PICodeBlockType >> inferer [
	^ inferer
]

{ #category : #accessing }
PICodeBlockType >> inferer: anObject [
	inferer := anObject
]

{ #category : #initialization }
PICodeBlockType >> initialize [
	super initialize.
	variables := Dictionary new.
	messagesTypes := OrderedCollection new.
]

{ #category : #variables }
PICodeBlockType >> messagesTypes [
	^ messagesTypes
]

{ #category : #variables }
PICodeBlockType >> messagesTypes: aMessageType [
	messagesTypes := aMessageType
]

{ #category : #variables }
PICodeBlockType >> needProcessing [
	^ variables anySatisfy: #needProcessing
]

{ #category : #accessing }
PICodeBlockType >> node [
	^ node
]

{ #category : #accessing }
PICodeBlockType >> node: anObject [
	node := anObject
]

{ #category : #accessing }
PICodeBlockType >> receiver [
	^ receiver
]

{ #category : #accessing }
PICodeBlockType >> receiver: anObject [
	receiver := anObject
]

{ #category : #accessing }
PICodeBlockType >> returnType [
	^ returnType
]

{ #category : #accessing }
PICodeBlockType >> returnType: anObject [
	returnType := anObject
]

{ #category : #variables }
PICodeBlockType >> variableAssociationAt: aName ifAbsent: aBlockClosure [ 
	^ variables associationAt: aName ifAbsent: aBlockClosure
]

{ #category : #variables }
PICodeBlockType >> variableAt: aName ifAbsent: aBlockClosure [ 
	^ variables at: aName ifAbsent: aBlockClosure
]

{ #category : #variables }
PICodeBlockType >> variableNamed: aName [ 
	^ self variableAt: aName ifAbsent: [ self homeContext resolveVariable: aName ]
]

{ #category : #variables }
PICodeBlockType >> variables [
	^ variables 
]

{ #category : #variables }
PICodeBlockType >> variablesNeedingProcessing [
	^ variables select:#needProcessing thenCollect:#value
]
