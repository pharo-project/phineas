Class {
	#name : #PISymbolType,
	#superclass : #PIImmutableType,
	#instVars : [
		'value',
		'concreteType'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PISymbolType >> accept: aVisitor [
	^ aVisitor visitSymbolType: self
]

{ #category : #testing }
PISymbolType >> asCType [
	^ CType new createFrom: self value
]

{ #category : #accessing }
PISymbolType >> concreteClass [
	^ concreteType concreteClass
]

{ #category : #accessing }
PISymbolType >> concreteType [
	^ concreteType
]

{ #category : #accessing }
PISymbolType >> concreteType: anObject [
	concreteType := anObject
]

{ #category : #variables }
PISymbolType >> instanceVariable: aName ifAbsent: aBlockClosure [ 
	^ concreteType instanceVariable: aName ifAbsent: aBlockClosure 
]

{ #category : #testing }
PISymbolType >> isSymbolType [
	^ true
]

{ #category : #printing }
PISymbolType >> printOn: aStream [
	aStream
		print: self class;
		nextPut: $(;
		print: value;
		nextPut: $)
]

{ #category : #accessing }
PISymbolType >> value [
	^ value
]

{ #category : #accessing }
PISymbolType >> value: anObject [
	value := anObject
]
