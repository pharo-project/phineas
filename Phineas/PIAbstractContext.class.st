Class {
	#name : #PIAbstractContext,
	#superclass : #Object,
	#instVars : [
		'returnType',
		'exceptionType'
	],
	#category : #'Phineas-Contexts'
}

{ #category : #accessing }
PIAbstractContext >> exceptionType [
	^ exceptionType
]

{ #category : #accessing }
PIAbstractContext >> exceptionType: anObject [
	exceptionType := anObject
]

{ #category : #initialization }
PIAbstractContext >> initialize [
	super initialize.
	exceptionType := PIVariableType new owner: self ; name: #exceptionType; yourself.
	returnType := PIVariableType new owner: self ; name: #returnType; yourself.
]

{ #category : #testing }
PIAbstractContext >> isRootContext [
	^self subclassResponsibility
]

{ #category : #accessing }
PIAbstractContext >> methodType [ 
	^ self subclassResponsibility
]

{ #category : #variables }
PIAbstractContext >> resolveVariable: aName [
	^ self subclassResponsibility 
]

{ #category : #accessing }
PIAbstractContext >> returnType [
	^ returnType
]

{ #category : #accessing }
PIAbstractContext >> returnType: anObject [
	returnType := anObject
]
