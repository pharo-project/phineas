Class {
	#name : #PIMessageType,
	#superclass : #PIImmutableType,
	#instVars : [
		'message',
		'receiver',
		'arguments',
		'typeProvider',
		'homeContext',
		'methodTypesDictionary',
		'receivedMessages'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIMessageType >> accept: aVisitor [
	^ aVisitor visitMessageType: self
]

{ #category : #flattening }
PIMessageType >> addReceivedMessage: otherMessage [
	receivedMessages add: otherMessage
]

{ #category : #activations }
PIMessageType >> addReturnType: aReturnType for: aPIMethodActivation [
	methodTypesDictionary at: aPIMethodActivation put: aReturnType
]

{ #category : #helpers }
PIMessageType >> allCombinations: aCollection [
	| first tail childCombinations |
	
	aCollection ifEmpty: [ ^ #(()) ].
	
	first := aCollection first.
	tail := aCollection allButFirst.
	
	tail ifEmpty: [ ^ first collect:[:e | {e}] ].
	
	childCombinations := self allCombinations: tail.
	
	^ first flatCollect: [ :e | childCombinations collect: [ :x | { e } , x ] ]
]

{ #category : #accessing }
PIMessageType >> arguments [
	^ arguments
]

{ #category : #accessing }
PIMessageType >> arguments: aCollection [ 
	arguments := aCollection
]

{ #category : #activations }
PIMessageType >> browse [
	self messageNode methodNode method browse
]

{ #category : #accessing }
PIMessageType >> callingMethod [
	self shouldBeImplemented.
]

{ #category : #accessing }
PIMessageType >> concreteClass [
	self assert: self concreteTypes size = 1.
	^ self concreteTypes anyOne concreteClass
]

{ #category : #accessing }
PIMessageType >> concreteClasses [
	
	^ self concreteTypes collect: #concreteClass
]

{ #category : #accessing }
PIMessageType >> concreteType [

	self error: 'Undefined for PIMessageType'
]

{ #category : #flattening }
PIMessageType >> concreteTypeVisited: visited receivedMessages: allReceived [
	(visited includes: self)
		ifTrue: [ ^ #() ].

	^ (typeProvider variableType: methodTypesDictionary values)
		concreteTypeVisited: visited
		receivedMessages: allReceived , receivedMessages
]

{ #category : #accessing }
PIMessageType >> concreteTypes [

	^ self returnVariableType concreteTypes
]

{ #category : #accessing }
PIMessageType >> hasTypeForActivation: aMethodActivation [

	^ methodTypesDictionary includesKey: aMethodActivation
]

{ #category : #accessing }
PIMessageType >> homeContext [
	^ homeContext
]

{ #category : #accessing }
PIMessageType >> homeContext: aPIContext [ 
	homeContext := aPIContext
]

{ #category : #initialization }
PIMessageType >> initialize [
	super initialize.
	methodTypesDictionary := Dictionary new.
	receivedMessages := OrderedCollection new.

]

{ #category : #testing }
PIMessageType >> isEmpty [
	
	^ self concreteTypes isEmpty
]

{ #category : #testing }
PIMessageType >> isMessageType [
	
	^ true
]

{ #category : #accessing }
PIMessageType >> message [
	^ message
]

{ #category : #accessing }
PIMessageType >> message: aRBMessageNode [ 
	message := aRBMessageNode
]

{ #category : #accessing }
PIMessageType >> messageNode [
	
	^ message
]

{ #category : #activations }
PIMessageType >> methodActivations [
	| allReceiverClasses allArgTypes |
	allReceiverClasses := receiver concreteTypes select:[ :ct | (ct concreteClass lookupSelector: message selector) isNotNil ].
	allArgTypes := arguments collect: #concreteTypes.	
	
	"If the message is ifNil: it only has meaning to analyze if it is in nil."	
	(allReceiverClasses isEmpty and:[#(isNil notNil ifNil: ifNil:ifNotNil:) includes: message selector ]) ifTrue: [ 
		allReceiverClasses add: typeProvider nilType ].
	
	^ allReceiverClasses flatCollect: [ :receiverType | 
		(self allCombinations: allArgTypes) collect: [ :argumentTypes |
			PIMethodActivation new
				typeProvider: typeProvider;
				receiver: receiverType;
				arguments: argumentTypes;
				message: self;
				yourself ]]
]

{ #category : #accessing }
PIMessageType >> methodType [
	
	^ self homeContext type
]

{ #category : #accessing }
PIMessageType >> methodTypes [

	^ self methodActivations collect: [ :each |
		each lookupMethodType ]
]

{ #category : #initialization }
PIMessageType >> methodTypesDictionary [
	^ methodTypesDictionary

]

{ #category : #testing }
PIMessageType >> needsProcessing [
	^ self methodActivations anySatisfy: [ :e | e hasType not ]
]

{ #category : #printing }
PIMessageType >> printOn: aStream visited: visited [
	"Old version, kept just in case"
	aStream
		nextPutAll: self class name;
		nextPut: $(;
		print: message;
		nextPutAll: ', '.

	(visited includes: self) ifFalse: [  
		visited add: self.
		methodTypesDictionary ifEmpty: [ 
			aStream nextPutAll: '<not yet calculated>' ].
		methodTypesDictionary	valuesDo: [ :t | 
			t printOn: aStream visited: visited.
			aStream nextPut: Character space ].	
	] ifTrue: [  
		aStream nextPutAll: '...'
	].

	aStream nextPut: $)
]

{ #category : #printing }
PIMessageType >> printOnInner: aStream [

		"nextPutAll: self class name;
		nextPutAll: '(';"
	self readable: receiver concreteTypes on: aStream.
	aStream << ' >> ';
		print: message selector;
		nextPutAll: ' -> '.
	self readable: self concreteTypes on: aStream.
			"	nextPut: $)"
]

{ #category : #accessing }
PIMessageType >> receiver [
	^ receiver
]

{ #category : #accessing }
PIMessageType >> receiver: aReceiverType [
	receiver := aReceiverType
]

{ #category : #accessing }
PIMessageType >> returnVariableType [

	^ typeProvider variableType: methodTypesDictionary values
]

{ #category : #accessing }
PIMessageType >> selector [
	^ message selector
]

{ #category : #accessing }
PIMessageType >> typeProvider [
	^ typeProvider
]

{ #category : #accessing }
PIMessageType >> typeProvider: aTypeProvider [
	typeProvider := aTypeProvider
]

{ #category : #querying }
PIMessageType >> types [
	
	^ methodTypesDictionary values flatCollect: #types
]

{ #category : #printing }
PIMessageType >> wasNotTyped [
	^ methodTypesDictionary isEmpty.
]

{ #category : #accessing }
PIMessageType >> wdttcf [
"	owner inferer typeProvider messageTypes select:[:aMethod| (aMethod concreteClasses includes: String) and: [ self includes: aMethod ] ] "
	PITypeInspector new
    root: self;
    openWithSpec;
    yourself.
]
