Class {
	#name : #PICollectionType,
	#superclass : #PIAbstractConcreteType,
	#instVars : [
		'collectionType',
		'elementType'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PICollectionType >> accept: aVisitor [
	^ aVisitor visitCollectionType: self
]

{ #category : #variables }
PICollectionType >> classVariable: aName ifPresent: aBlock [ 
	^ collectionType classVariable: aName ifPresent: aBlock 
]

{ #category : #accessing }
PICollectionType >> collectionType [
	^ collectionType 
]

{ #category : #accessing }
PICollectionType >> collectionType: aConcreteType [
	collectionType := aConcreteType 
]

{ #category : #accessing }
PICollectionType >> concreteClass [
	^ collectionType concreteClass
]

{ #category : #consolidating }
PICollectionType >> consolidateWith: aCollection [
	" A collection type consolidate with other PICollectionType if the type of the collection is the same, and it returns a copy with all the elements"

	| otherCollectionType newType |
	
	aCollection ifEmpty: [ aCollection add: self. ^ aCollection ].
		
	otherCollectionType := aCollection detect: [ :x | x collectionType = self collectionType ] ifNone: [ aCollection add: self. ^ aCollection ].	
				
	self elementType types do: [ :e | otherCollectionType elementType addType: e ].
	
	self becomeForward: otherCollectionType.
	
	^ aCollection
]

{ #category : #accessing }
PICollectionType >> elementType [
	^ elementType
]

{ #category : #accessing }
PICollectionType >> elementType: aType [
	aType owner: self.
	aType name: #value.
	elementType := aType
]

{ #category : #initializing }
PICollectionType >> fillType: aTypesProvider fromObject: aCollection [ 
	aCollection do: [ :each |
		elementType addType: (aTypesProvider typeForObject: each) ]
]

{ #category : #variables }
PICollectionType >> initialize [
	super initialize.
	elementType := PIVariableType new owner: self ; name: #elementType ; yourself.
]

{ #category : #variables }
PICollectionType >> instanceVariable: aName ifAbsent: aBlockClosure [ 
	^ collectionType instanceVariable: aName ifAbsent: aBlockClosure 
]

{ #category : #testing }
PICollectionType >> isCollectionType [
	^ true
]

{ #category : #printing }
PICollectionType >> printOnInner: aStream [
		self readable: collectionType concreteTypes on: aStream.
		aStream nextPutAll: ' of '.
		self readable: elementType concreteTypes on: aStream.
]

{ #category : #querying }
PICollectionType >> types [
	
	^ {collectionType} 
]

{ #category : #variables }
PICollectionType >> variableAt: aName ifAbsent: aBlockClosure [ 
	^ collectionType variableAt: aName ifAbsent: aBlockClosure 
]
