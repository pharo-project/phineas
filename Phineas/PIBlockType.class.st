Class {
	#name : #PIBlockType,
	#superclass : #PICodeBlockType,
	#instVars : [
		'homeContext',
		'isCalculated'
	],
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIBlockType >> accept: aVisitor [
	^ aVisitor visitBlockType: self
]

{ #category : #'calculating types' }
PIBlockType >> calculateTypes [

	^ self calculateTypesWith: #()
]

{ #category : #'calculating types' }
PIBlockType >> calculateTypesWith: args [
	
	isCalculated ifTrue:[ ^ returnType ].

	isCalculated := true.	
	self inferer stack push: (self inferer createBlockContextFor: self withArguments: args).
					
	[ returnType addType: (node body acceptVisitor: (inferer visitor methodType: self ; yourself)).
	  ^ returnType ]
		ensure: [ self inferer stack pop ]
]

{ #category : #accessing }
PIBlockType >> concreteClass [
	^ BlockClosure
]

{ #category : #accessing }
PIBlockType >> homeContext [
	^ homeContext
]

{ #category : #accessing }
PIBlockType >> homeContext: anObject [
	homeContext := anObject
]

{ #category : #initialization }
PIBlockType >> initialize [
	super initialize.
	returnType := PIVariableType new owner: self ; name: #aReturnType; yourself.
	isCalculated := false.
]

{ #category : #testing }
PIBlockType >> isBlockType [
	^ true
]

{ #category : #testing }
PIBlockType >> isFor: aBlockNode in: aHomeContext [
	^ node = aBlockNode and: [ homeContext methodType = aHomeContext methodType ]
]

{ #category : #accessing }
PIBlockType >> methodType [
	^ creatingNodeContext methodType
]

{ #category : #printing }
PIBlockType >> printOnInner: aStream [
	^ aStream
		<< self class name;
		<< '(';
		<< self node printString;
		<< ')'
]

{ #category : #accessing }
PIBlockType >> reset [

	isCalculated := false
]
