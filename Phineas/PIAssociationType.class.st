Class {
	#name : #PIAssociationType,
	#superclass : #PIAbstractConcreteType,
	#category : #'Phineas-Types'
}

{ #category : #visiting }
PIAssociationType >> accept: aVisitor [
	^ aVisitor visitAssociationType: self
]

{ #category : #accessing }
PIAssociationType >> concreteClass [
	^ Association
]

{ #category : #consolidating }
PIAssociationType >> consolidateWith: aCollection [
	| otherType |
	aCollection
		ifEmpty: [ aCollection add: self.
			^ aCollection ].
	self assert: aCollection size = 1.
	otherType := aCollection anyOne.
	self valueType typeProvider do: [ :e | otherType valueType addType: e ].
	self keyType typeProvider do: [ :e | otherType keyType addType: e ].
	self becomeForward: otherType.
	^ aCollection
]

{ #category : #variables }
PIAssociationType >> fillType: typesProvider fromObject: assoc [
	self valueType addType: (typesProvider typeForObject: assoc value).
	self keyType addType: (typesProvider typeForObject: assoc key)
]

{ #category : #accessing }
PIAssociationType >> initialize [ 
	super initialize.
	instanceVariables at: #key put: (PIVariableType new owner:self ; name: #keyType ; yourself).
	instanceVariables at: #value put: (PIVariableType new owner:self ; name: #valueType ; yourself).
]

{ #category : #testing }
PIAssociationType >> isAssociationType [
	^ true
]

{ #category : #accessing }
PIAssociationType >> keyType [
	^ instanceVariables at: #key
]

{ #category : #accessing }
PIAssociationType >> keyType: aType [
	aType owner: self.
	aType name: #key.
	instanceVariables at: #key put: aType
	
]

{ #category : #printing }
PIAssociationType >> printOnInner: aStream [
	self readable: self keyType concreteTypes on: aStream.
	aStream << ' -> '.
	self readable: self keyType concreteTypes on: aStream.
	
]

{ #category : #accessing }
PIAssociationType >> types [
	^{ self keyType. self valueType }
]

{ #category : #accessing }
PIAssociationType >> valueType [
	 ^ instanceVariables at: #value
]

{ #category : #accessing }
PIAssociationType >> valueType: aType [
	aType owner: self.
	aType name: #value.
	instanceVariables at: #value put: aType
]
