Class {
	#name : #PIFFIHeuristic,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIFFIHeuristic >> applyToMessage: aMessageNode receiver: aReceiver arguments: arguments [
	| selector method ffiNode functionDefinition |
	selector := aMessageNode selector.
	method := aReceiver concreteClass lookupSelector: selector.

	ffiNode := method ast allChildren
		detect: [ :e | e isMessage and: [ self ffiSelectors includes: e selector ] ].

	functionDefinition := FFIFunctionParser new parseNamedFunction: ffiNode arguments first value.
	
	^ (self validateArgumentsOf: method ast with: arguments inFunction: functionDefinition) 
		ifTrue: [ self translateType: functionDefinition returnType ]
		ifFalse: [ self inferer invalidType ]
]

{ #category : #'as yet unclassified' }
PIFFIHeuristic >> basicCreateMethodTypeForActivation: aPIMethodActivation [ 

	^ PIFFIMethodType new
		inferer: self inferer;
		node: aPIMethodActivation methodNode;
		creatingNode: self inferer currentContext;
		receiver: aPIMethodActivation receiver;
		fillArguments: aPIMethodActivation arguments;
		fillTemporaries;
		yourself
]

{ #category : #accessing }
PIFFIHeuristic >> ffiSelectors [
	^ #(ffiCall: #ffiCall:module: #ffiCall:options: #ffiCall:module:options:)
]

{ #category : #testing }
PIFFIHeuristic >> isApplicableToMessage: aMessageNode receiver: aReceiver arguments: arguments [ 
	| selector method |
	selector := aMessageNode selector.
	method := aReceiver concreteClass lookupSelector: selector.
	
	method ifNil: [ ^false ].
	
	^ (method ast allChildren select: [ :e | e isMessage ]) anySatisfy: [ :e | self ffiSelectors includes: e selector ].
]

{ #category : #'converting-types' }
PIFFIHeuristic >> translateType: aDef [ 
	
	| typeName arity |
	
	typeName := aDef name.
	arity := aDef arity.
	
	arity > 0 ifTrue: [ self halt: 'Not implemented' ].
	
	typeName = #int ifTrue: [ ^ self inferer typeForClass: SmallInteger ].
	
	^ self inferer typeForClass: typeName asClass.
]

{ #category : #validating }
PIFFIHeuristic >> validateArgumentsOf: aMethodNode with: arguments inFunction: functionSpec [
	| argumentsWithName |
	argumentsWithName := ((aMethodNode arguments collect: #name) with: arguments collect: [ :name :arg | name -> arg ])
		asDictionary.

	^ (functionSpec arguments select: #isArray)
		allSatisfy: [ :def | 
			| arg |
			arg := argumentsWithName at: def first.
			arg isOfType: (self translateType: (def last: 2)) concreteClass ]
]
