Class {
	#name : #PIBasicNewHeuristic,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIBasicNewHeuristic >> applyToMessage: aMessage receiver: aReceiver arguments: arguments [
	^ self inferer typeForClass: aReceiver concreteClass instanceSide
]

{ #category : #testing }
PIBasicNewHeuristic >> isApplicableToMessage: aMessage receiver: aReceiver arguments: arguments [

	aReceiver concreteClass isClass ifFalse: [ ^ false ].
	aMessage selector = #basicNew ifTrue: [ ^ true ].
	"The VM object has #free message to release the memory"
	aMessage selector = #freeObject ifTrue: [ ^ true ].	
	
	^ aMessage selector = #basicNew: and: [ (arguments at: 1) isOfType: Integer ]
]
