Class {
	#name : #PIDictionaryHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIDictionaryHeuristics >> applyToMessage: aMessage receiver: aReceiver arguments: args [ 
	| type |
	(aMessage selector = #size) 
		ifTrue:[ ^ heuristics inferer typeForClass: SmallInteger ].
	
	(aMessage selector = #new: and: [aReceiver isOfType: Dictionary class]) 
		ifTrue:[ ^ self handleNew ].
		
	(aMessage selector = #->) ifTrue: [ type := self typeProvider createAssociationType.
		type keyType addType: aReceiver.
		type valueType addType: args first.
		^ type	
	].

	(#(at: at:put: at:ifAbsent: at:ifAbsentPut:) includes: aMessage selector) ifTrue: [ 
		^ self handleAtPut: aMessage receiver: aReceiver args: args].

	aMessage selector = #add: ifTrue: [ 
		"It receives an association as parameter"
		aReceiver valueType addType: args first valueType.
		aReceiver keyType addType: args first keyType.
		^ args first ].

	(#(includesKey: includesValue: includes:) includes: aMessage selector) ifTrue: [ ^self typeProvider typeForClass: Boolean].
	(#(initialize: yourself) includes: aMessage selector) ifTrue: [ ^ aReceiver ].
	
	self halt.

]

{ #category : #handling }
PIDictionaryHeuristics >> handleAtPut: message receiver: receiver args: args [ 
	message selector = #at: ifTrue:[
		receiver keyType addType: args first.
		^ receiver valueType.
	].

	message selector = #at:put: ifTrue:[
		receiver keyType addType: args first.
		receiver valueType addType: args second.
		^ receiver valueType.
	].

	message selector = #at:ifAbsent: ifTrue:[
		receiver keyType addType: args first.
		args second isBlockType 
			ifTrue: [ ^ args second calculateTypes ].
			
		receiver valueType addType: args second.
		^ receiver valueType.
	].

	message selector = #at:ifAbsentPut: ifFalse:[^self ].
	
	receiver keyType addType: args first.
	args second isBlockType 
		ifTrue: [ receiver valueType addType: args second calculateTypes ]
		ifFalse: [ receiver valueType addType: args second. ].
	^ receiver valueType.
]

{ #category : #cases }
PIDictionaryHeuristics >> handleNew [
	^ heuristics inferer typeProvider createDictionaryType
]

{ #category : #'special types' }
PIDictionaryHeuristics >> handleSpecialTypes: aClass [
	(aClass includesBehavior: Dictionary)
		ifTrue: [ ^ heuristics inferer typeProvider createDictionaryType ].
	(aClass includesBehavior: Association)
		ifTrue: [ ^ heuristics inferer typeProvider createAssociationType ].
	^ nil
]

{ #category : #testing }
PIDictionaryHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [
	^ aMessage selector = #new:
		and: [ (aReceiver isOfType: Dictionary class)
				and: [ aMessage selector = #-> 
					and: [ aReceiver isOfType: Dictionary ] ] ]
]
