Class {
	#name : #PIAsHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIAsHeuristics >> applyToMessage: aRBMessageNode receiver: aReceiver arguments: args [
	| collectionType |
	collectionType := self inferer
		typeForClass: args anyOne concreteClass instanceSide.
	(collectionType isDictionaryType and: [ aReceiver isCollectionType ])
		ifFalse: [ collectionType elementType addType: aReceiver elementType ].
	^ collectionType
]

{ #category : #testing }
PIAsHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: aCollection [
	^ aMessage selector = #as: 
]
