Class {
	#name : #PIContext,
	#superclass : #PIAbstractInnerContext,
	#category : #'Phineas-Contexts'
}

{ #category : #accessing }
PIContext >> methodType [
	^ type
]

{ #category : #printing }
PIContext >> printOn: aStream [
	aStream
		nextPutAll: self class name;
		nextPut: $(;
		nextPutAll: type node methodNode methodClass name;
		nextPutAll: ' >> ';
		nextPutAll: type node methodNode selector;
		nextPutAll: ' (';
		print: self receiver;
		nextPut: $);
		nextPutAll: ' (';
		print: self methodType arguments;
		nextPut: $);	
		nextPut: $)
]

{ #category : #accessing }
PIContext >> receiver [
	^ self type receiver
]

{ #category : #variables }
PIContext >> resolveVariable: aName [
	^ type variableAt: aName
		ifAbsent: [ type receiver
				variableAt: aName
				ifAbsent: [ parent resolveVariable: aName ] ]
]

{ #category : #accessing }
PIContext >> returnType [
	^ type returnType
]
