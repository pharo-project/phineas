Class {
	#name : #UnresolvedBinding,
	#superclass : #Error,
	#instVars : [
		'unboundVariable'
	],
	#category : #'Phineas-Errors'
}

{ #category : #accessing }
UnresolvedBinding >> description [
	^ String streamContents: [ :s|
		s << 'UnresolvedBinding for:'
			print: unboundVariable
		]
]

{ #category : #accessing }
UnresolvedBinding >> unboundVariable [
	^ unboundVariable
]

{ #category : #accessing }
UnresolvedBinding >> unboundVariable: aSymbol [
	unboundVariable := aSymbol
]
