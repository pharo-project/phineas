Class {
	#name : #PIMethodType,
	#superclass : #PICodeBlockType,
	#category : #'Phineas-Types'
}

{ #category : #accessing }
PIMethodType >> concreteClass [
	^ CompiledMethod
]

{ #category : #accessing }
PIMethodType >> homeContext [
	^ creatingNodeContext
]

{ #category : #initialization }
PIMethodType >> initialize [
	super initialize.
	returnType := PIVariableType new owner: self ; name: #aReturnType; yourself.
]

{ #category : #testing }
PIMethodType >> isForMethodNode: aMethodNode receiver: aReceiver with: aCollection [ 
	^ node = aMethodNode and: [ receiver = aReceiver and: [ self arguments hasEqualElements: aCollection ] ]
]

{ #category : #printing }
PIMethodType >> printOnInner: aStream [
	aStream
		print: self class;
		nextPut: $(;
		print: node;
		nextPut: $)
]

{ #category : #accessing }
PIMethodType >> selector [
	^ self node selector
]

{ #category : #querying }
PIMethodType >> typeOfBlock: aRBBlockNode [
	| types |
	types := inferer typeProvider blockTypes
		select: [ :each | each methodType = self and: [ each node = aRBBlockNode ] ].
	self assert: types size = 1.
	^ types anyOne
]

{ #category : #querying }
PIMethodType >> typeOfMessage: aRBMessageNode [
	| types |
	types := inferer typeProvider messageTypes
		select:
			[ :each | each methodType = self and: [ each messageNode = aRBMessageNode ] ].
	self assert: types size = 1.
	^ types anyOne
]
