Class {
	#name : #PIMethodInferer,
	#superclass : #Object,
	#instVars : [
		'inferer'
	],
	#category : #Phineas
}

{ #category : #apply }
PIMethodInferer class >> on: aPhineasInferer [ 
	
	^ self new
		inferer: aPhineasInferer;
		yourself
]

{ #category : #applying }
PIMethodInferer >> applyTo: aPIMethodActivation [ 
	| methodAST returnType myContext methodType |

	methodAST := aPIMethodActivation methodNode. 
	aPIMethodActivation lookupMethodType ifNotNil: [ :x | 
		aPIMethodActivation returnType: x returnType.
		^ x returnType ].

	methodType := inferer typeProvider
		methodTypeForMethodActivation: aPIMethodActivation
		ifAbsentPut: [
			PISmalltalkMethodType new
				node: methodAST;
				creatingNode: inferer currentContext;
				receiver: aPIMethodActivation receiver;
				inferer: inferer;
				getPragmasInformations;
				fillArguments: aPIMethodActivation arguments;
				fillTemporaries;
				yourself ].

	myContext := inferer createContextForMethodType: methodType.
	inferer stack push: myContext.
	returnType := myContext returnType.
	aPIMethodActivation returnType: returnType.
	
	methodAST acceptVisitor: (inferer visitor methodType: methodType ; yourself).
	methodAST hasReturn ifFalse: [ returnType addType: aPIMethodActivation receiver ].
	
	inferer stack pop.
	^ returnType
]

{ #category : #accessing }
PIMethodInferer >> inferer: aPhineasInferer [ 
	inferer := aPhineasInferer
]
