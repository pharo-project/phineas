Class {
	#name : #UntreatableType,
	#superclass : #Error,
	#instVars : [
		'type'
	],
	#category : #'Phineas-Errors'
}

{ #category : #accessing }
UntreatableType >> description [
	^ String streamContents:[:s| 
		s << 'The ''type'' isn''t the right format : ';
			print: type.
		]
]

{ #category : #accessing }
UntreatableType >> type [
	^ type
]

{ #category : #accessing }
UntreatableType >> type: aType [
	type := aType
]
