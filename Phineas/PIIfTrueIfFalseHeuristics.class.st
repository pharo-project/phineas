Class {
	#name : #PIIfTrueIfFalseHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIIfTrueIfFalseHeuristics >> applyToMessage: aMessage receiver: receiver arguments: args [
	| returnType |
	
	returnType := PIVariableType new owner: self ; name: #aReturnType; yourself.
	
	args size = 1
		ifTrue: [ returnType addType: (self typeProvider nilType) ].
			
	args do: [ :e | e isBlockType 
		ifTrue:[

			e calculateTypes.
			returnType addType: e returnType ]
		ifFalse:[ returnType addType: e ] ].
	^ returnType
]

{ #category : #applying }
PIIfTrueIfFalseHeuristics >> basicCreateMethodTypeForActivation: aPIMethodActivation [ 

	^ PIIfTrueIfFalseMethodType new
		inferer: self inferer;
		node: aPIMethodActivation methodNode;
		creatingNode: self inferer currentContext;
		receiver: aPIMethodActivation receiver;
		fillArguments: aPIMethodActivation arguments;
		fillTemporaries;
		yourself
]

{ #category : #testing }
PIIfTrueIfFalseHeuristics >> isApplicableToMessage: aMessage receiver: receiver arguments: arguments [
	^ (receiver isOfType: Boolean)
		and: [ #(#ifTrue: #ifFalse: #ifTrue:ifFalse: #ifFalse:ifTrue:) includes: aMessage selector ]
]
