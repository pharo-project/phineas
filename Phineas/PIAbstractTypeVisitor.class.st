"
I represent an abstract visitor for PIMethodTypes.

I just dispatch a visitor on the right type of method. 
(no recursive call)

visitor := PIAbstractMethodVisitor new.
aMethodType acceptVsitor: visitor.

"
Class {
	#name : #PIAbstractTypeVisitor,
	#superclass : #Object,
	#category : #'Phineas-Visitor'
}

{ #category : #visiting }
PIAbstractTypeVisitor >> visitAssociationType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitBlockType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitCollectionType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitConcreteMetaType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitConcreteType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitDictionaryType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitExplicitType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitFFIMethodType: aFFIMethodType [

]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitIfTrueIfFalseMethodType: anIfTrueIfFalseMethodType [

]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitMessageType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitMethodType: aMethodType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitPrimitiveMethodType: aPrimitiveMethodType [

]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitSmallIntegerMethodType: aSmallIntegerMethodType [


]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitSmalltalkMethodType: aSmalltalkMethodType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitSymbolType: aType [
]

{ #category : #visiting }
PIAbstractTypeVisitor >> visitVariableType: aType [
]
