Class {
	#name : #NoTypeFound,
	#superclass : #Error,
	#instVars : [
		'something'
	],
	#category : #'Phineas-Errors'
}

{ #category : #accessing }
NoTypeFound >> description [
	^ String streamContents: [ :s | 
		s << 'No type was found by the type inferer for: ';
			print: something 
		]
]

{ #category : #accessing }
NoTypeFound >> something [
	^ something
]

{ #category : #accessing }
NoTypeFound >> something: aSomething [
	something := aSomething
]
