Class {
	#name : #PIExceptionHeuristics,
	#superclass : #PIAbstractHeuristic,
	#category : #'Phineas-Heuristics-Messages'
}

{ #category : #applying }
PIExceptionHeuristics >> applyToMessage: aMessage receiver: aReceiver arguments: args [ 
	self inferer currentContext exceptionType addType: aReceiver.
	^ aReceiver.
]

{ #category : #testing }
PIExceptionHeuristics >> isApplicableToMessage: aMessage receiver: aReceiver arguments: args [
	^ (#(signal signal:) includes: aMessage selector) and: [ aReceiver isOfType: Exception ]
]
