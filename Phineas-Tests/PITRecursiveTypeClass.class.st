Class {
	#name : #PITRecursiveTypeClass,
	#superclass : #Object,
	#instVars : [
		'instVarHoldingOldObject'
	],
	#category : #'Phineas-Tests-general'
}

{ #category : #accessing }
PITRecursiveTypeClass >> recurse [
	
	| currentObject oldClass |
	currentObject := nil.
	oldClass := currentObject class.
	currentObject := 'String'.
	^ oldClass
]

{ #category : #accessing }
PITRecursiveTypeClass >> recurseWith: anObject [
	
	| oldObject currentObject result |
	oldObject := currentObject.
	result := oldObject yourself.
	currentObject := anObject.
	^ result
]

{ #category : #accessing }
PITRecursiveTypeClass >> recurseWithInstVar: aPITSimpleAccessorsClass [
	
	| oldContext |
	oldContext := instVarHoldingOldObject.
	aPITSimpleAccessorsClass anInstanceVariable: oldContext.
	instVarHoldingOldObject := aPITSimpleAccessorsClass.
]
