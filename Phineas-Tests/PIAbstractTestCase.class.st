Class {
	#name : #PIAbstractTestCase,
	#superclass : #TestCase,
	#instVars : [
		'inferer'
	],
	#category : #'Phineas-Tests-general'
}

{ #category : #asserting }
PIAbstractTestCase >> assertAssociationType: aType is: association [
	| type |
	self assert: aType size equals: 1.
	type := aType anyOne.
	
	self assert: type isAssociationType.
	self assert: type keyType concreteClasses equals: {association key} asSet.
	self assert: type valueType concreteClasses equals: {association value} asSet.	
]

{ #category : #asserting }
PIAbstractTestCase >> assertCollectionType: aType is: type with: elementTypes [

	self assertType: aType is: type.
	self assertCollection: ((aType collect: #elementType) flatCollect: #concreteClasses) equals: elementTypes asSet
]

{ #category : #asserting }
PIAbstractTestCase >> assertDictionaryType: aType key: keyClass value: valueClass [
	| concreteType |
	self assert: aType size equals: 1.
	
	concreteType := aType anyOne.
	self assert: concreteType isDictionaryType.
	self assert: concreteType concreteClass equals: Dictionary.
	
	
]

{ #category : #asserting }
PIAbstractTestCase >> assertType: type is: expected [
	
	| concreteClasses |
	concreteClasses := type isCollection
		ifTrue: [ type flatCollect: #concreteClasses ]
		ifFalse: [ type concreteClasses ].
	concreteClasses isEmpty ifTrue: [ self error: 'Phineas didn''t find any type' ].
	concreteClasses size = 1
		ifTrue: [ self assert: concreteClasses anyOne equals: expected ]
		ifFalse: [ self assertCollection: concreteClasses equals: expected asSet ]
]

{ #category : #assertions }
PIAbstractTestCase >> assertTypeIsEmpty: aType [
	self assert: aType concreteTypes size = 0
]

{ #category : #running }
PIAbstractTestCase >> assertTypeOf: aNode is: expected [
	| pharoType |
	pharoType := (inferer typeOfNode: aNode) concreteTypes.
	pharoType isEmpty ifTrue: [ self error: 'Phineas didn''t find any type' ].
	pharoType size = 1
		ifTrue: [ self assert: pharoType anyOne concreteClass equals: expected ]
		ifFalse: [ self assertCollection: (pharoType collect: [:e | e concreteClass]) equals: expected asSet ]
]

{ #category : #running }
PIAbstractTestCase >> inferMethodSelector: aSelector from: aClass [
	| aMethodAST |
	aMethodAST := (aClass >> aSelector) ast.

	inferer infer: aMethodAST.
	
	^ aMethodAST
]

{ #category : #running }
PIAbstractTestCase >> inferMethodSelector: aSelector from: aClass usingBlock: aBlock [
	| aMethodAST |
	aMethodAST := (aClass >> aSelector) ast.
	inferer infer: aBlock.

	^ aMethodAST
]

{ #category : #running }
PIAbstractTestCase >> setUp [
	super setUp.
	inferer := PhineasInferer new.
]
