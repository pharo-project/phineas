Class {
	#name : #PITSimpleInitializedClass,
	#superclass : #Object,
	#instVars : [
		'anInstanceVariable',
		'anotherInstanceVariable',
		'thirdVariable'
	],
	#category : #'Phineas-Tests-general'
}

{ #category : #initialization }
PITSimpleInitializedClass >> initialize [
	super initialize.
	anInstanceVariable := 1.
	anotherInstanceVariable := $c.
	thirdVariable := 3.5
]
