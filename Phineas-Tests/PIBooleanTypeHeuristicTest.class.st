Class {
	#name : #PIBooleanTypeHeuristicTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-message-heuristics'
}

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testAnd [
	self assertType: (inferer infer: [ true and: [ false ] ]) is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testAnd3Inner [
	self
		assertType: (inferer infer: [ true and: [ false and: [ true ] ] ])
		is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testAnd3Outer [
	self
		assertType: (inferer infer: [ (true and: [ false ]) and: [ true ] ])
		is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testAndBinary [
	self assertType: (inferer infer: [ true & false ]) is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testAndBinary3 [
	self assertType: (inferer infer: [ true & false & true ]) is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testNot [
	self assertType: (inferer infer: [ true not ]) is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testNotNot [
	self assertType: (inferer infer: [ true not not ]) is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testOr [
	self assertType: (inferer infer: [ true or: [ false ] ]) is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testOr3Inner [
	self
		assertType: (inferer infer: [ true or: [ false and: [ true ] ] ])
		is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testOr3Outer [
	self
		assertType: (inferer infer: [ (true or: [ false ]) or: [ true ] ])
		is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testOrBinary [
	self assertType: (inferer infer: [ true | false ]) is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testOrBinary3 [
	self assertType: (inferer infer: [ true | false | true ]) is: Boolean
]

{ #category : #tests }
PIBooleanTypeHeuristicTest >> testXor [
	self assertType: (inferer infer: [ true xor: [ false ] ]) is: Boolean
]
