Class {
	#name : #PIIfNilHeuristicsTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-message-heuristics'
}

{ #category : #tests }
PIIfNilHeuristicsTest >> testIfNil [
	
	self assertType: (inferer infer: [ nil ifNil:[ 1 ] ]) is: {UndefinedObject. SmallInteger}.
]

{ #category : #tests }
PIIfNilHeuristicsTest >> testIfNilIfNotNil [
	
	self assertType: (inferer infer: [ nil ifNil:[ 1 ] ifNotNil: [ 2 ] ]) is: SmallInteger.
]

{ #category : #tests }
PIIfNilHeuristicsTest >> testIfNilReceivedByBoolean [
	self assertType: (inferer infer: [ true ifNil:[ 1 ] ]) is: {Boolean. SmallInteger}.
]

{ #category : #tests }
PIIfNilHeuristicsTest >> testIfNilReceivedByNil [
	self assertType: (inferer infer: [ nil ifNil:[ 1 ] ]) is: {UndefinedObject. SmallInteger}.
]

{ #category : #tests }
PIIfNilHeuristicsTest >> testIfNotNil [
	
	self assertType: (inferer infer: [ nil ifNotNil:[ 1 ] ]) is: {UndefinedObject. SmallInteger}.
]

{ #category : #tests }
PIIfNilHeuristicsTest >> testIfNotNilIfNil [
	
	self assertType: (inferer infer: [ nil ifNotNil:[ 1 ] ifNil: [ 2 ] ]) is: SmallInteger.
]

{ #category : #tests }
PIIfNilHeuristicsTest >> testIfNotNilReceivedByBoolean [
	self assertType: (inferer infer: [ true ifNotNil:[ 1 ] ]) is: {Boolean. SmallInteger}.
]

{ #category : #tests }
PIIfNilHeuristicsTest >> testIfNotNilReceivedByNil [
	self assertType: (inferer infer: [ nil ifNotNil:[ 1 ] ]) is: {UndefinedObject. SmallInteger}.
]
