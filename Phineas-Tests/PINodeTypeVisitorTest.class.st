Class {
	#name : #PINodeTypeVisitorTest,
	#superclass : #PIAbstractTestCase,
	#instVars : [
		'iv'
	],
	#classVars : [
		'A'
	],
	#classInstVars : [
		'b'
	],
	#category : #'Phineas-Tests-accessing'
}

{ #category : #support }
PINodeTypeVisitorTest class >> aMethodWithAClassVariable [
	b:=1
]

{ #category : #support }
PINodeTypeVisitorTest >> aBlockWithIntegerAsLastStatement [
	[ 4 ]
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodThatReturnAMessageThatReturnsAnInt [
	^ 1 + 2
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodThatReturnsAnInteger [
	^ 4
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithABlockWithATemporary [
	^ [ | i | i := 3 ] value
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithABlockWithAnArgument [
	^ [:i| ] value: 1
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithAClassVariable [
	A := 3
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithAGlobalVariable [
	PINodeTypeVisitorTest
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithALiteralNode [
	1
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithATemporaryVariable [
	| t |
	t := 3
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithAnArgument: anInt [
	^ anInt
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithAnAssignement [
	| e |
	e := 3 
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithAnInstanceVariable [
	iv := 3
]

{ #category : #support }
PINodeTypeVisitorTest >> aMethodWithSelf [
	self
]

{ #category : #running }
PINodeTypeVisitorTest >> testGetInstanceVariableType [
	| type |
	"Not semantically in the right class, but kinda bundle together for now"
	self inferMethodSelector: #aMethodWithAnInstanceVariable from: self class.
	type := inferer typeOfInstanceVariable: #iv ofClass: self class.
	self assert: type equals: { SmallInteger } asSet.
]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitArgumentNode [
	| ast counter |
	counter := 0.
	ast := self inferMethodSelector: #aMethodWithAnArgument: from: self class usingBlock:[ PINodeTypeVisitorTest new aMethodWithAnArgument: 3 ].
	ast nodesDo: [ :aNode |
		aNode isArgumentVariable
			ifTrue: [ 
				counter := counter + 1.
				self assertTypeOf: aNode is: SmallInteger.
			].].
	self assert: counter equals: 2.
]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitArgumentNodeInABlock [
	| ast counter |
	counter := 0.
	ast := self
		inferMethodSelector: #aMethodWithABlockWithAnArgument
		from: self class.
	ast
		nodesDo: [ :aNode | 
			aNode isArgumentVariable
				ifTrue: [ counter := counter + 1.
					self assertTypeOf: aNode is: SmallInteger ] ].
	self assert: counter equals: 1
]

{ #category : #test }
PINodeTypeVisitorTest >> testVisitAssignmentNode [

	| ast counter |
	counter := 0.
	ast := self
		inferMethodSelector: #aMethodWithAnAssignement 
		from: self class.
	ast
		nodesDo: [ :aNode | 
			aNode isAssignment
				ifTrue: [ counter := counter + 1.
					self assertTypeOf: aNode is: SmallInteger ] ].
	self assert: counter equals: 1
]

{ #category : #test }
PINodeTypeVisitorTest >> testVisitBlockNode [

	| ast counter |
	counter := 0.
	ast := self
		inferMethodSelector: #aBlockWithIntegerAsLastStatement 
		from: self class.
	ast nodesDo: [ :aNode | 
			aNode isBlock
				ifTrue: [ counter := counter + 1.
					self assertTypeOf: aNode is: SmallInteger ] ].
	self assert: counter equals: 1
]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitGlobalNodeClassVariable [
	| ast counter |
	counter := 0.
	ast := self inferMethodSelector: #aMethodWithAClassVariable from: self class.
	ast nodesDo: [ :aNode | 
		(aNode isVariable and:[aNode isClassVariable])
			ifTrue: [ 
				counter := counter + 1. 

				self assertTypeOf: aNode is: {SmallInteger. UndefinedObject}.
				"unless it's set in the call graph, the class variable might be unset"
			].].
	self assert: counter equals:1.

]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitGlobalNodeGlobal [
	| ast counter|
	ast := self inferMethodSelector: #aMethodWithAGlobalVariable from: self class.
	counter:=0.
	ast nodesDo: [ :aNode | 
		(aNode isVariable and:[aNode isGlobalVariable])
			ifTrue: [ 
				counter := counter + 1.
				self assertTypeOf: aNode is: self class.
			].].
		
	self assert: counter equals: 1
]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitGlobalNodeSharedPoolVariable [
	"waiting for a better understanding of SharedPool variables"
	<expectedFailure>
	self flag: #toImplement.
	self assert: false equals: true.
]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitInstanceVariableClassSide [
	| ast counter |
	counter := 0.
	ast := self inferMethodSelector: #aMethodWithAClassVariable from: self class class.
	ast nodesDo: [ :aNode | 
		(aNode isVariable and:[aNode isInstanceVariable])
			ifTrue: [ 
				counter := counter + 1. 

				self assertTypeOf: aNode is: SmallInteger.
			].].
	self assert: counter equals:1.

]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitInstanceVariableNode [
	| ast counter |
	counter:=0.
	ast := self inferMethodSelector: #aMethodWithAnInstanceVariable from: self class.
	ast nodesDo: [ :aNode | 
		(aNode isVariable and:[ aNode isInstanceVariable ])
			ifTrue: [ 
				counter := counter + 1.
				self assertTypeOf: aNode is: SmallInteger.
			].].
	self assert: counter equals: 1.

]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitLiteralValueNode [
	| ast counter |
	counter:=0.
	ast := self inferMethodSelector: #aMethodWithALiteralNode from: self class.
	ast nodesDo: [ :aNode | 
		(aNode isLiteralNode and:[ aNode isValue ])
			ifTrue: [ 
				counter := counter + 1.
				self assertTypeOf: aNode is: SmallInteger.
			]].
	self assert: counter equals: 1.

]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitMessageNode [
	| ast counter |
	counter := 0.
	ast := self
		inferMethodSelector: #aMethodThatReturnAMessageThatReturnsAnInt
		from: self class.
	ast
		nodesDo: [ :aNode | 
			aNode isMessage
				ifTrue: [ counter := counter + 1.
					self assertTypeOf: aNode is: SmallInteger ] ].
	self assert: counter equals: 1
]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitMethodNode [
	| ast |
	ast := self inferMethodSelector: #aMethodThatReturnsAnInteger from: self class.
	self assertTypeOf: ast is: SmallInteger.

]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitSelfNode [
	| ast counter |
	counter := 0.
	ast := self inferMethodSelector: #aMethodWithSelf from: self class.
	ast nodesDo: [ :aNode | 
		aNode isSelfVariable
			ifTrue: [ 
				counter := counter + 1.
				self assertTypeOf: aNode is: self class.
			].].
	self assert: counter equals:1.

]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitTemporaryNode [
	| ast counter |
	counter := 0.
	ast := self inferMethodSelector: #aMethodWithATemporaryVariable from: self class.
	ast nodesDo: [ :aNode | 
		aNode isTempVariable 
			ifTrue: [ 
				counter := counter + 1.
				self assertTypeOf: aNode is: SmallInteger.
				].].
	self assert: counter equals:2.
]

{ #category : #running }
PINodeTypeVisitorTest >> testVisitTemporaryNodeInABlock [
	| ast counter |
	counter := 0.
	ast := self inferMethodSelector: #aMethodWithABlockWithATemporary from: self class.
	ast nodesDo: [ :aNode | 
		aNode isTempVariable
			ifTrue: [ 
				counter := counter + 1.
				self assertTypeOf: aNode is: SmallInteger.
				].].
	self assert: counter equals:2.
]
