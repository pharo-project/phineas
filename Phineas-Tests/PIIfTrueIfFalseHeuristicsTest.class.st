Class {
	#name : #PIIfTrueIfFalseHeuristicsTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-message-heuristics'
}

{ #category : #tests }
PIIfTrueIfFalseHeuristicsTest >> testReturnTypeOfIfFalse [
	
	self assertType: (inferer infer: [ true ifFalse:[ 3 ] ]) is: {SmallInteger. UndefinedObject}.
	
]

{ #category : #tests }
PIIfTrueIfFalseHeuristicsTest >> testReturnTypeOfIfFalseIfTrue [
	
	self assertType: (inferer infer: [ true ifFalse:[ 3 ] ifTrue:[ 2 ] ]) is: SmallInteger.
	
]

{ #category : #tests }
PIIfTrueIfFalseHeuristicsTest >> testReturnTypeOfIfTrue [
	
	self assertType: (inferer infer: [ true ifTrue:[ 3 ] ]) is: {SmallInteger. UndefinedObject}.
	
]

{ #category : #tests }
PIIfTrueIfFalseHeuristicsTest >> testReturnTypeOfIfTrueIfFalse [
	
	self assertType: (inferer infer: [ true ifTrue:[ 3 ] ifFalse:[ 2 ] ]) is: SmallInteger.
	
]
