Class {
	#name : #PITypeConsolidationTest,
	#superclass : #PIAbstractTestCase,
	#instVars : [
		'typesProvider'
	],
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PITypeConsolidationTest >> setUp [
	super setUp.
	
	typesProvider := inferer typeProvider.
]

{ #category : #tests }
PITypeConsolidationTest >> testCollectionTypes [
	| tt t1 t2 t3 t4 t5 tc1 tc2 tc3 tc4 tr1 tr2 |
	tt := typesProvider emptyType.
	t1 := typesProvider typeForClass: SmallInteger.
	t2 := typesProvider typeForClass: ByteString.
	t3 := typesProvider typeForClass: Object.
	t4 := typesProvider typeForClass: Float.
	t5 := typesProvider typeForClass: CompiledMethod.
	
	tc1 := typesProvider typeForClass: Array.
	tc2 := typesProvider typeForClass: OrderedCollection.
	tc3 := typesProvider typeForClass: Array.
	tc4 := typesProvider typeForClass: OrderedCollection.

	tc1 elementType addType: t1.
	tc1 elementType addType: t3.
	tc3 elementType addType: t5.

	tc2 elementType addType: t2.
	tc4 elementType addType: t4.

	tt addType: tc1.
	tt addType: tc2.
	tt addType: tc3.
	tt addType: tc4.

	tt consolidateTypes.

	self assert: tt concreteTypes size equals: 2.

	tr1 := tt concreteTypes detect: [ :e | e collectionType = tc1 collectionType ].
	tr2 := tt concreteTypes detect: [ :e | e collectionType = tc2 collectionType ].

	self assertCollection: tr1 elementType concreteTypes hasSameElements: { t1. t3. t5 }.
	self assertCollection: tr2 elementType concreteTypes hasSameElements: { t2. t4 }	
]

{ #category : #tests }
PITypeConsolidationTest >> testConcreteTypes [
	| tt t1 t2 t3 |
	tt := typesProvider emptyType.
	t1 := typesProvider typeForClass: SmallInteger.
	t2 := typesProvider typeForClass: ByteString.
	t3 := typesProvider typeForClass: Object.

	tt addType: t1.
	tt addType: t2.
	tt addType: t3.

	self
		assertCollection: tt concreteTypes
		hasSameElements: {t1. t2. t3}
]

{ #category : #tests }
PITypeConsolidationTest >> testDictionaryTypes [
	| tt t1 t2 t3 t4 t5 td1 td2 |
	tt := typesProvider emptyType.
	t1 := typesProvider typeForClass: SmallInteger.
	t2 := typesProvider typeForClass: ByteString.
	t3 := typesProvider typeForClass: Object.
	t4 := typesProvider typeForClass: Float.
	t5 := typesProvider typeForClass: CompiledMethod.
	
	td1 := typesProvider typeForClass: Dictionary.
	td2 := typesProvider typeForClass: Dictionary.

	td1 keyType addType: t1.
	td2 keyType addType: t3.
	td2 keyType addType: t5.

	td1 valueType addType: t2.
	td2 valueType addType: t4.

	tt addType: td1.
	tt addType: td2.
	
	tt consolidateTypes.

	self assert: tt concreteTypes size equals: 1.
	self assertCollection: tt concreteType keyType concreteTypes hasSameElements: { t1. t3. t5 }.
	self assertCollection: tt concreteType valueType concreteTypes hasSameElements: { t2. t4 }	
]
