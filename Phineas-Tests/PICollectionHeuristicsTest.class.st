Class {
	#name : #PICollectionHeuristicsTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-message-heuristics'
}

{ #category : #tests }
PICollectionHeuristicsTest >> testAddFirstOnArray [
	self assertTypeIsEmpty: (inferer inferBlockClosure:[ {} addFirst: 1 ])
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddFirstOnEmpty [
	self assertType: (inferer inferBlockClosure:[ {} asOrderedCollection addFirst: 1 ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddFirstWithDifferentTypes [
	self assertType: (inferer inferBlockClosure:[ { $q } asOrderedCollection addFirst: 1 ]) is: {Character. SmallInteger}
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddFirstWithSameTypes [
	self assertType: (inferer inferBlockClosure:[ { 1 } asOrderedCollection addFirst: 1 ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddLastOnArray [
	self assertTypeIsEmpty: (inferer inferBlockClosure:[ {} addFirst: 1 ])
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddLastOnEmpty [
	self assertType: (inferer inferBlockClosure:[ {} asOrderedCollection addFirst: 1 ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddLastWithDifferentTypes [
	self assertType: (inferer inferBlockClosure:[ { $q } asOrderedCollection addFirst: 1 ]) is: {Character. SmallInteger}
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddLastWithSameTypes [
	self assertType: (inferer inferBlockClosure:[ { 1 } asOrderedCollection addFirst: 1 ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddOnEmpty [
	self assertType: (inferer inferBlockClosure:[ {} add: 1 ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddWithDifferentTypes [
	self assertType: (inferer inferBlockClosure:[ { $a } add: 1 ]) is: {Character. SmallInteger}
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAddWithSameType [
	self assertType: (inferer inferBlockClosure:[ { 1 } add: 1 ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAt [
	self assertType: (inferer inferBlockClosure:[ { $a } at: 1 ]) is: Character
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAtPutDifferentType [
	self assertType: (inferer inferBlockClosure:[ { $a } at: 1 put: 1]) is: {Character.SmallInteger}
]

{ #category : #tests }
PICollectionHeuristicsTest >> testAtPutSamePutType [
	self assertType: (inferer inferBlockClosure:[ { $a } at: 1 put: $m ]) is: Character
]

{ #category : #tests }
PICollectionHeuristicsTest >> testCollectBlockYourself [
	self assertCollectionType: (inferer inferBlockClosure:[ { $a } collect: [:anElem| anElem yourself]]) concreteTypes is: Array with: {Character}
]

{ #category : #tests }
PICollectionHeuristicsTest >> testCollectSymbolYourself [
	self assertCollectionType: (inferer inferBlockClosure:[ { $a } collect: #yourself]) concreteTypes is: Array with: {Character}
]

{ #category : #tests }
PICollectionHeuristicsTest >> testDetectBlock [
	self assertType: (inferer inferBlockClosure: [ {1} detect: [ :anElem | anElem even ] ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testDetectSymbol [
	self assertType: (inferer inferBlockClosure:[ { 1 } detect: #even ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testFirst [
	self assertType: (inferer inferBlockClosure:[ { 1 } first ]) is: SmallInteger 
]

{ #category : #tests }
PICollectionHeuristicsTest >> testRemoveFirst [
	"Doesn't work on any of the other supported collections but LinkedList"
	self assertTypeIsEmpty: (inferer inferBlockClosure:[ { 1 } removeFirst ])
]

{ #category : #tests }
PICollectionHeuristicsTest >> testRemoveFirstOnLinkedList [
	"Doesn't work on any of the other supported collections"
	self assertType: (inferer inferBlockClosure:[ LinkedList new add: 1 ; removeFirst ]) is: SmallInteger
]

{ #category : #tests }
PICollectionHeuristicsTest >> testSize [
	self assertType: (inferer inferBlockClosure:[ { 1 } size ]) is: SmallInteger 
]

{ #category : #tests }
PICollectionHeuristicsTest >> testSort [
	self assertCollectionType: (inferer inferBlockClosure:[ { 1 } sort ]) concreteTypes is: Array with: { SmallInteger }
]
