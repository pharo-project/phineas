Class {
	#name : #PIProgramTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PIProgramTest >> testSumNumbers [
	| type ast types |

	ast := (BasicPrograms >> #printMixingTypes).
	inferer inferRootMethod: ast.
	
	types := inferer typesOfMethod: ast selector from: ast methodClass.
	
	self assert: types size equals: 1.
	
	type := types anyOne.
	
	self assertType: type returnType is: SmallInteger.
	self assertType: (type variableNamed: #io) is: BasicIO.
	self assertType: (type variableNamed: #result) is: SmallInteger.
	
]
