Class {
	#name : #PIRecursiveAssignmentTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PIRecursiveAssignmentTest >> testRecurseWithArguments [
	self assertType: (inferer infer: [PITRecursiveTypeClass new recurseWith: 'a string' ]) is: ByteString
]

{ #category : #tests }
PIRecursiveAssignmentTest >> testRecurseWithInstanceVariable [

	inferer infer: [PITRecursiveTypeClass new recurseWithInstVar: PITSimpleAccessorsClass new].

	self
		assertType: ((inferer typeForClass: PITSimpleAccessorsClass) instanceVariable: #anInstanceVariable)
		is: PITSimpleAccessorsClass
]

{ #category : #tests }
PIRecursiveAssignmentTest >> testRecurseWithoutArguments [
	self assertType: (inferer infer: [PITRecursiveTypeClass new recurse]) is: {ByteString class. UndefinedObject class}
]
