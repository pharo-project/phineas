Class {
	#name : #PISimpleImplicitReturnTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> aMethodThatReturnsMiddleMethod [
	| aVar |
	aVar := 3 + 3.
	#() ifEmpty: [ ^ 3 ].
	aVar even
]

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> aMethodWithoutAReturn [
	|aVar|
	aVar:=3 + 3.
	aVar even
]

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> anEmptyMethod [
]

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> testASTImplicitReturn [
	| methodNode |
	methodNode := self inferMethodSelector: #anEmptyMethod from: self class.
	self assertTypeOf: methodNode is:  self class
]

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> testASTNoReturn [
	| methodNode |
	methodNode := self
		inferMethodSelector: #aMethodWithoutAReturn
		from: self class.
	self assertTypeOf: methodNode is: self class
]

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> testASTReturnInTheMiddleOfMethod [
	| methodNode |
	methodNode := self
		inferMethodSelector: #aMethodThatReturnsMiddleMethod
		from: self class.
	self
		assertTypeOf: methodNode
		is:
			{self class.
			SmallInteger}
]

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> testBlockImplicitReturn [
	self
		assertType: (inferer infer: [ PISimpleImplicitReturnTest new anEmptyMethod ])
		is: self class
]

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> testBlockNoReturn [
	self
		assertType: (inferer infer: [ PISimpleImplicitReturnTest new aMethodWithoutAReturn ])
		is: self class
]

{ #category : #'tests-blocks' }
PISimpleImplicitReturnTest >> testBlockReturnInTheMiddleOfMethod [
	self
		assertType:
			(inferer
				infer: [ PISimpleImplicitReturnTest new aMethodThatReturnsMiddleMethod ])
		is:
			{self class.
			SmallInteger}
]
