Class {
	#name : #PISimpleClassTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PISimpleClassTest >> testInstSpec [
	self assertType: (inferer infer: [Object instSpec]) is: SmallInteger 
]
