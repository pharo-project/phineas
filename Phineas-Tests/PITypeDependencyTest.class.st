Class {
	#name : #PITypeDependencyTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PITypeDependencyTest >> testMessageType [
	| type |
	inferer := PhineasInferer new.
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	type := inferer typeOf: (RBParser parseExpression: #'x + 1').	
	
	self assert: type isMessageType
]

{ #category : #tests }
PITypeDependencyTest >> testMessageTypeWithUnresolvedReceiverHasNoType [
	| type |
	inferer := PhineasInferer new.
	
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	type := inferer typeOf: (RBParser parseExpression: 'y + 1').
		
	self assert: type isEmpty.
	self deny:   type needsProcessing.
]

{ #category : #tests }
PITypeDependencyTest >> testResolveReceiverAfterMessageDefinesType [
	| type |
	inferer := PhineasInferer new.
	
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	type := inferer typeOf: (RBParser parseExpression: 'y + 1').
	inferer typeOf: (RBParser parseExpression: 'y := 1').

	self assert: type needsProcessing.
	self assert: type isEmpty.
]

{ #category : #tests }
PITypeDependencyTest >> testResolveReceiverAfterMessageDefinesTypeAndProcessMethod [
	| type |
	inferer := PhineasInferer new.
	
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	type := inferer typeOf: (RBParser parseExpression: 'y + 1').
	inferer typeOf: (RBParser parseExpression: 'y := 1').
	
	self assert: type needsProcessing.
	self assert: type isEmpty.
]

{ #category : #tests }
PITypeDependencyTest >> testVariableAssignmentType [
	| type integerType |
	inferer := PhineasInferer new.
	
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	type := inferer typeOf: (RBParser parseExpression: 'x := 1').
	integerType := inferer typeOf: (RBParser parseExpression: '1').
	
	self assert: type isVariableType.
	self assert: (type includes: integerType).
]

{ #category : #tests }
PITypeDependencyTest >> testVariableAssignmentTypeIsMessageType [
	| assignmentType messageType |
	inferer := PhineasInferer new.
	
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	assignmentType := inferer typeOf: (RBParser parseExpression: 'x := y + 1').
	
	self assert: assignmentType isVariableType.
	self assert: assignmentType isEmpty.
	
	messageType := assignmentType types anyOne.
	self deny: messageType needsProcessing.
]

{ #category : #tests }
PITypeDependencyTest >> testVariableAssignmentTypeIsVariableType [
	| variableType assignmentType |
	inferer := PhineasInferer new.
	
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	variableType := inferer typeOf: (RBParser parseExpression: 'x').
	assignmentType := inferer typeOf: (RBParser parseExpression: 'x := 1').
	
	self assert: (assignmentType includes: variableType).
]

{ #category : #tests }
PITypeDependencyTest >> testVariableType [
	| type |
	inferer := PhineasInferer new.
	
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	type := inferer typeOf: (RBVariableNode named: #'x').
	
	self assert: type isVariableType.
	self assert: type isEmpty.
]

{ #category : #tests }
PITypeDependencyTest >> testVariableTypeIsAlwaysSameType [
	| type anotherType |
	inferer := PhineasInferer new.
	
	inferer
		pushContextWithMethod: (Point>>#x) ast
		receiver: (inferer typeForClass: Point).
	
	type := inferer typeOf: (RBVariableNode named: #'x').
	anotherType := inferer typeOf: (RBVariableNode named: #'x').
	
	self assert: type == anotherType
]
