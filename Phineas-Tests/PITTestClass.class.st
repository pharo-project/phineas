Class {
	#name : #PITTestClass,
	#superclass : #Object,
	#category : #'Phineas-Tests-general'
}

{ #category : #'as yet unclassified' }
PITTestClass >> aNonLocalReturn: aValue [
	aValue = 0 ifTrue: [ ^ 3 ] ifFalse: [ ^ $c ].
]

{ #category : #'as yet unclassified' }
PITTestClass >> doAsString: anObject [

	^ anObject asString
]
