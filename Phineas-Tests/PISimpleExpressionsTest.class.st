Class {
	#name : #PISimpleExpressionsTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PISimpleExpressionsTest >> testAssociations [
	self assertAssociationType: (inferer infer: [2 -> 3]) is: SmallInteger -> SmallInteger.
	self assertAssociationType: (inferer infer: ['hola' -> $c]) is: ByteString -> Character.
	self assertAssociationType: (inferer infer: [-1.1 -> 2]) is: Float -> SmallInteger.

]

{ #category : #tests }
PISimpleExpressionsTest >> testBlockEvaluation [
	self assertType: (inferer infer: [[1 + 1] value]) is: SmallInteger.
	self assertType: (inferer infer: [[ :a | a ] value: 3]) is: SmallInteger.
	self assertType: (inferer infer: [[ :a :b | b ] value: 3 value: $s]) is: Character.
	self assertType: (inferer infer: [[ :a :b | b + a] value: 3 value: 4]) is: SmallInteger.

	self assertType: (inferer infer: [[ | a | a := 1. a ] value]) is: SmallInteger
]

{ #category : #tests }
PISimpleExpressionsTest >> testByteStringNew [
	self assertType: (inferer infer: [ByteString new: 5]) is: ByteString.

]

{ #category : #tests }
PISimpleExpressionsTest >> testComparionOfNumbers [
	self assertType: (inferer infer: [1.0 = 1]) is: Boolean.
	self assertType: (inferer infer: [0 > 0.0]) is: Boolean.
	self assertType: (inferer infer: [-1.1 < 3]) is: Boolean.

]

{ #category : #tests }
PISimpleExpressionsTest >> testIntegerAsString [
	self assertType: (inferer infer: [ 3 asString , '4']) is: ByteString

]

{ #category : #tests }
PISimpleExpressionsTest >> testSimpleIf [
	self assertType: (inferer infer: [2 > 3 ifTrue:[ $a ]]) is: {UndefinedObject. Character}.
	self assertType: (inferer infer: [2 > 3 ifFalse:[ $a ]]) is: {UndefinedObject. Character}.
	self assertType: (inferer infer: [nil ifNotNil:[ $a ]]) is: {UndefinedObject. Character}.
	self assertType: (inferer infer: [3 ifNotNil:[ $a ]]) is: {Character. SmallInteger}.

	self assertType: (inferer infer: [2 > 3 ifFalse:[ $a ] ifTrue:[ 1.0 ]]) is: {Character. Float}.
	self assertType: (inferer infer: [2 > 3 ifTrue:[ $a ] ifFalse:[ 1.0 ]]) is: {Character. Float}.
	
	self assertType: (inferer infer: [2 > 3 ifTrue: $a  ifFalse: 1.0]) is: {Character. Float}.
	

]

{ #category : #tests }
PISimpleExpressionsTest >> testSmallInteger [
	self assertType: (inferer infer: [1]) is: SmallInteger.
	self assertType: (inferer infer: [0]) is: SmallInteger.
	self assertType: (inferer infer: [-1]) is: SmallInteger
]

{ #category : #tests }
PISimpleExpressionsTest >> testStringAsInteger [
	self assertType: (inferer infer: ['23' asInteger + 3]) is: SmallInteger.

]

{ #category : #tests }
PISimpleExpressionsTest >> testSumOfSmallInteger [
	self assertType: (inferer infer: [1 + 1]) is: SmallInteger.
	self assertType: (inferer infer: [0 + 2]) is: SmallInteger.
	self assertType: (inferer infer: [-1 + 3]) is: SmallInteger.
]

{ #category : #tests }
PISimpleExpressionsTest >> testSumOfSmallIntegerAndFloat [
	self assertType: (inferer infer: [1.0 + 1]) is: Float.
	self assertType: (inferer infer: [0 + 0.0]) is: Float.
	self assertType: (inferer infer: [-1.1 + 3]) is: Float.

]
