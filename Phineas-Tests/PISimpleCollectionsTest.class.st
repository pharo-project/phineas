Class {
	#name : #PISimpleCollectionsTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PISimpleCollectionsTest >> testArrayAt [
	self assertType: (inferer infer: [{1. 2. 3} at: 1]) is: SmallInteger
]

{ #category : #tests }
PISimpleCollectionsTest >> testArrayCollect [
	self
		assertCollectionType: (inferer infer: [{1. 2. 3} collect: #yourself])
		is: Array
		with: {SmallInteger}
]

{ #category : #tests }
PISimpleCollectionsTest >> testArrayDo [	
	self assertType: (inferer infer: [{1. 2. 3} sumNumbers: #yourself]) is: SmallInteger
]

{ #category : #tests }
PISimpleCollectionsTest >> testArrayDoWithDifferentTypes [	
	self assertCollectionType: (inferer infer: [{1. 2. 3. $a. $b} collect: #yourself]) 
		is: Array with: {SmallInteger. Character}
]

{ #category : #tests }
PISimpleCollectionsTest >> testArrayOfSmallIntegers [

	self assertCollectionType: (inferer infer: {1}) is: Array with: {SmallInteger}.
	self assertCollectionType: (inferer infer: {0. 1. 3.}) is: Array with: {SmallInteger}.
	self assertCollectionType: (inferer infer: {0. 1. {3}}) is: Array with: {SmallInteger. Array}.

	self assertCollectionType: (inferer infer: [{1}]) is: Array with: {SmallInteger}.
	self assertCollectionType: (inferer infer: [{0. 1. 3.}]) is: Array with: {SmallInteger}.
	self assertCollectionType: (inferer infer: [{0. 1. {3}}]) is: Array with: {SmallInteger. Array}.
]

{ #category : #tests }
PISimpleCollectionsTest >> testAsDictionary [
	self assertDictionaryType: (inferer infer: [{ 'one'->1. 'two'->2. 'three'->3} asDictionary]) key: String value: SmallInteger
]

{ #category : #tests }
PISimpleCollectionsTest >> testAsOrderedCollection [	

	self assertCollectionType: (inferer infer: [{1. 2. 3} asOrderedCollection]) is: OrderedCollection with: { SmallInteger }.
	self assertType: (inferer infer: [{1. 2. 3} asOrderedCollection sumNumbers: #yourself]) is: SmallInteger.
]

{ #category : #tests }
PISimpleCollectionsTest >> testAsOrderedCollectionCollect [
	self assertCollectionType: 
	(inferer infer: [{1. 2. 3} asOrderedCollection collect: #yourself]) 
	is: OrderedCollection with: {SmallInteger}
]

{ #category : #tests }
PISimpleCollectionsTest >> testStaticArray [
	self assertCollectionType: (inferer infer: [ #(1 2 3) ]) is: Array with: {SmallInteger}
]

{ #category : #tests }
PISimpleCollectionsTest >> testStaticByteArray [
	self assertType: (inferer infer: [ #[1 2 3] ]) is: ByteArray
]
