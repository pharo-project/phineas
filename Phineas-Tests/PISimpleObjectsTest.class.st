Class {
	#name : #PISimpleObjectsTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PISimpleObjectsTest >> testArrayCreation [
	self assertCollectionType: (inferer infer: [Array basicNew: 23]) is: Array with: {}.
	self assertCollectionType: (inferer infer: [Array new: 23]) is: Array with: {}.

]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralArrayWithManyTypes [
	
	self assertCollectionType: (inferer infer: [ #( 1 $a ) ]) is: Array with: {SmallInteger . Character}
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralArrayWithSingleType [
	
	self assertCollectionType: (inferer infer: [ #( 1 2 3 ) ]) is: Array with: {SmallInteger}
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralBoolean [
	
	self assertType: (inferer infer: [ true ]) is: Boolean
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralByteArray [
	
	self assertType: (inferer infer: [ #[ 0 1 2 3 ] ]) is: ByteArray
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralCharacter [
	
	self assertType: (inferer infer: [ $a ]) is: Character
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralDynamicArray [
	
	self assertCollectionType: (inferer infer: [ { 1+1 . 'asd' first } ]) is: Array with: {SmallInteger . Character}
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralFloat [
	
	self assertType: (inferer infer: [ 2.5 ]) is: Float
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralSmallInteger [
	
	self assertType: (inferer infer: [ 1 ]) is: SmallInteger
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralString [
	
	self assertType: (inferer infer: [ '1' ]) is: ByteString
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralSymbol [
	
	self assertType: (inferer infer: [ #asd ]) is: ByteSymbol
]

{ #category : #tests }
PISimpleObjectsTest >> testLiteralUndefinedObject [
	
	self assertType: (inferer infer: [ nil ]) is: UndefinedObject
]

{ #category : #tests }
PISimpleObjectsTest >> testNonLocalReturn [
	self assertType: (inferer infer: [PITTestClass new aNonLocalReturn: 3])
		is: {SmallInteger.Character}
]

{ #category : #tests }
PISimpleObjectsTest >> testObjectComparison [
	self assertType: (inferer infer: [Object new = 22]) is: Boolean.

]

{ #category : #tests }
PISimpleObjectsTest >> testObjectComparisonDifference [
	self assertType: (inferer infer: [Object new ~= 22]) is: Boolean.

]

{ #category : #tests }
PISimpleObjectsTest >> testObjectCreation [
	self assertType: (inferer infer: [Object]) is: Object class.
	self assertType: (inferer infer: [Object basicNew]) is: Object.
	self assertType: (inferer infer: [Object new]) is: Object.
]

{ #category : #tests }
PISimpleObjectsTest >> testPerform [
	self
		assertType:
			(inferer
				infer: [ | a |
					a := #new.
					OrderedCollection perform: a ])
		is: OrderedCollection
]

{ #category : #tests }
PISimpleObjectsTest >> testRecursiveMethod [
	self assertType: (inferer infer: [3 factorial]) is: SmallInteger
]
