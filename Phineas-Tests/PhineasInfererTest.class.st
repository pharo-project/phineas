"
A PhineasInfererTest is a test class for testing the behavior of PhineasInferer
"
Class {
	#name : #PhineasInfererTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #test }
PhineasInfererTest >> testEntryPoints [
	inferer inferBlockClosure: [ 1 + 1 ].
	self assert: inferer entryPoints size  equals: 1.
	
	inferer inferBlockClosure: [ #Druss ].
	self assert: inferer entryPoints size  equals: 2.
]
