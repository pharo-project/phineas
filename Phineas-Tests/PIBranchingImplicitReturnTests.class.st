Class {
	#name : #PIBranchingImplicitReturnTests,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #'tests-blocks' }
PIBranchingImplicitReturnTests >> aMethodThatReturnsInOneBranch [
	#() ifNotEmpty:[:anElement| ^ 1 ]
]

{ #category : #'tests-blocks' }
PIBranchingImplicitReturnTests >> testASTOnlyOneBranchReturns [
	<expectedFailure>
	| methodNode |
	methodNode := self
		inferMethodSelector: #aMethodThatReturnsInOneBranch
		from: self class.
	self
		assertTypeOf: methodNode
		is:
			{self class.
			SmallInteger}
]

{ #category : #'tests-blocks' }
PIBranchingImplicitReturnTests >> testBlockOnlyOneBranchReturns [
	<expectedFailure>
	self
		assertType:
			(inferer
				infer: [ PIBranchingImplicitReturnTests new aMethodThatReturnsInOneBranch ])
		is:
			{self class.
			SmallInteger}
]
