Class {
	#name : #PITSimpleAccessorsClass,
	#superclass : #Object,
	#instVars : [
		'anInstanceVariable',
		'anotherInstanceVariable',
		'thirdVariable'
	],
	#category : #'Phineas-Tests-general'
}

{ #category : #accessing }
PITSimpleAccessorsClass >> anInstanceVariable [
	^ anInstanceVariable
]

{ #category : #accessing }
PITSimpleAccessorsClass >> anInstanceVariable2: anObject [
 	| x |
	x := anObject.
	anInstanceVariable := x
]

{ #category : #accessing }
PITSimpleAccessorsClass >> anInstanceVariable: anObject [
	anInstanceVariable := anObject
]

{ #category : #accessing }
PITSimpleAccessorsClass >> anotherInstanceVariable [
	^ anotherInstanceVariable
]

{ #category : #accessing }
PITSimpleAccessorsClass >> anotherInstanceVariable: anObject [
	anotherInstanceVariable := anObject
]

{ #category : #accessing }
PITSimpleAccessorsClass >> thirdVariable [
	^ thirdVariable
]

{ #category : #accessing }
PITSimpleAccessorsClass >> thirdVariable: anObject [
	thirdVariable := anObject
]
