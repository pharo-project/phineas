Class {
	#name : #PIAssociationHeuristicTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-message-heuristics'
}

{ #category : #tests }
PIAssociationHeuristicTest >> testKey [
	self assertType: (inferer infer: [ ( 1 -> 2) key] ) is: SmallInteger
]
