Class {
	#name : #PIInstanceVariableTest,
	#superclass : #PIAbstractTestCase,
	#category : #'Phineas-Tests-general'
}

{ #category : #tests }
PIInstanceVariableTest >> testAccessedClass [
	| type expressionType |
	expressionType := inferer
		infer: [ PITSimpleAccessorsClass new anInstanceVariable: 1; yourself ].
	type := inferer typeForClass: PITSimpleAccessorsClass.

	self assertType: (type instanceVariable: #anInstanceVariable) is: SmallInteger.
	self assert: expressionType equals: type concreteTypes
]

{ #category : #tests }
PIInstanceVariableTest >> testInitializedClass [
	| type |
	inferer infer: [ PITSimpleInitializedClass new ].
	type := inferer typeForClass: PITSimpleInitializedClass.

	self assertType: (type instanceVariable: #anInstanceVariable) is: SmallInteger. 
	self assertType: (type instanceVariable: #anotherInstanceVariable) is: Character. 
	self assertType: (type instanceVariable: #thirdVariable) is: Float. 
]

{ #category : #tests }
PIInstanceVariableTest >> testTemporary [
	| type expressionType |
	expressionType := inferer
		infer: [PITSimpleAccessorsClass new anInstanceVariable2: 1; yourself].
	type := inferer typeForClass: PITSimpleAccessorsClass.

	self assertType: (type instanceVariable: #anInstanceVariable) is: SmallInteger.
	self assert: expressionType equals: type concreteTypes
]
